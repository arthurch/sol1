#include <solk/video/video.h>

void video_init(boot_t *kb, psf2_header_t *psf_h)
{
  /* only linear framebuffers are supported */
  lfb_video_init(kb);
  ioctl_vid(NULL, IO_VID_NATIVE_FONT, psf_h); /* load psf font */
}

int load_psf_font(psf_font_t *font, psf2_header_t *psf_h)
{
  if(PSF2_MAGIC_OK(psf_h->magic))
  {
    /* PSF2 font */
    font->psf_font_start = (void *) psf_h + psf_h->headersize;

    font->length = psf_h->length;
    font->char_sz = psf_h->charsize;
    font->char_h = psf_h->height;
    font->char_w = psf_h->width;
    font->byteperline = (font->char_w+7)/8;
  } else if(PSF1_MAGIC_OK(((psf1_header_t *) psf_h)->magic))
  {
    /* PSF1 font */
    psf1_header_t *psf1_h = (psf1_header_t *) psf_h;

    font->psf_font_start = (void *) psf1_h + sizeof(struct psf1_header);
    unsigned int mode = psf1_h->mode;

    font->length = (mode & PSF1_MODE512) ? 512 : 256;
    font->char_h = psf1_h->charsize;
    font->char_w = 8;
    font->char_sz = psf1_h->charsize;
    font->byteperline = (font->char_w+7)/8;
  } else
  {
    return 0;
  }

  return 1;
}

uint32_t rgbatoi(unsigned char r, unsigned char g, unsigned char b, unsigned char a)
{
  return (uint32_t) ((uint32_t) r) << 16 | ((uint32_t) g) << 8 | (uint32_t) b | (uint32_t) a << 24;
}

uint32_t rgbtoi(unsigned char r, unsigned char g, unsigned char b)
{
  return (uint32_t) (((uint32_t) r) << 16 | ((uint32_t) g) << 8 | (uint32_t) b) | 0xFF000000;
}
