#include <solk/video/video.h>

#include <solk/kernel.h>
#include <solk/video/psf.h>
#include <solk/panic.h>
#include <solk/mm/vmm.h>

#include <errno.h>
#include <string.h>

uint16_t lfb_resolution_w = 0; /* Width */
uint16_t lfb_resolution_h = 0; /* Height */
uint16_t lfb_resolution_b = 0; /* Bits per pixel */
uint32_t lfb_resolution_p = 0; /* Pitch (number of bytes per row) */
/* Framebuffer addr (in virtual memoy) */
uint8_t *lfb_vid_base = (uint8_t *) FB_VIRTUAL_ADDR;
uint32_t lfb_vid_len = 0; /* Framebuffer length in memory */

psf_font_t lfb_font; /* font structure */

void lfb_video_init(boot_t *kb)
{
  if(kb->framebuffer_type != BOOT_FRAMEBUFFER_TYPE_RGB)
  {
    /* can't use this framebuffer */
    EARLY_PANIC();
    return;
  }

  lfb_resolution_w = kb->framebuffer_width;
  lfb_resolution_h = kb->framebuffer_height;
  lfb_resolution_b = kb->framebuffer_bpp;
  lfb_resolution_p = kb->framebuffer_pitch;

  lfb_vid_len = lfb_resolution_h * lfb_resolution_p;

  /* map framebuffer to virtual memory */
  pagemap_t fb_pm;
  fb_pm.status = PAGEMAP_NOT_USED;
  fb_pm.size = lfb_vid_len;
  fb_pm.base = (uintptr_t) lfb_vid_base;
  fb_pm.pagecount = (fb_pm.size + PAGE_MASK) / PAGESIZE;
  fb_pm.pages = (uintptr_t *) early_kmalloc(fb_pm.pagecount * sizeof(uintptr_t));
  for(size_t i = 0; i<fb_pm.pagecount; i++)
  {
    fb_pm.pages[i] = kb->framebuffer_addr + i*PAGESIZE;
  }
  map_pagemap(&fb_pm, PAGE_RW);

  memset(&lfb_font, 0, sizeof(struct psf_font));
}

/* change pixel color (32 bit) */
void set_point(unsigned int x, unsigned int y, uint32_t c)
{
  uint32_t *disp = (uint32_t *) lfb_vid_base;
  uint32_t *cell = &disp[y * (lfb_resolution_p / 4) + x];
  *cell = c;
}

/* 24 bit version */
void set_point24(unsigned int x, unsigned int y, uint32_t c)
{
  uint32_t *disp = (uint32_t *) lfb_vid_base;
  uint32_t *cell = &disp[y * (lfb_resolution_p / 3) + x];
  *cell = (c & 0x00FFFFFF) | (*cell & 0xFF000000);
}

/* clear framebuffer */
void lfb_clr()
{
  uint32_t *disp = (uint32_t *) lfb_vid_base;
  memset(disp, 0, lfb_vid_len);
}

int ioctl_vid(void *unused, unsigned long request, void *argp)
{
  (void) unused;

  switch(request)
  {
    case IO_VID_WIDTH:
      *((uint16_t *) argp) = lfb_resolution_w;
      break;
    case IO_VID_HEIGHT:
      *((uint16_t *) argp) = lfb_resolution_h;
      break;
    case IO_VID_BPP:
      *((uint16_t *) argp) = lfb_resolution_b;
      break;
    case IO_VID_PITCH:
      *((uint32_t *) argp) = lfb_resolution_p;
      break;
    case IO_VID_FB_BASE:
      *((uintptr_t *) argp) = (uintptr_t) lfb_vid_base;
      break;
    case IO_VID_CHAR_W:
      *((uint32_t *) argp) = lfb_font.char_w;
      break;
    case IO_VID_CHAR_H:
      *((uint32_t *) argp) = lfb_font.char_h;
      break;
    case IO_VID_NATIVE_FONT:
      if(argp == NULL)
      {
        lfb_font.psf_font_start = NULL;
      } else
      {
        psf_font_t last;
        memcpy(&last, &lfb_font, sizeof(struct psf_font));

        if(load_psf_font(&lfb_font, (psf2_header_t *) argp) == 0)
        {
          /* if loading the font failed, don't change the font */
          memcpy(&lfb_font, &last, sizeof(struct psf_font));
        }
      }
      break;
    default:
      __set_errno(EINVAL);
      return -1;
  }
  return 0;
}

/* display characters natively */
void write_char(unsigned char c, uint32_t x, uint32_t y, uint32_t fg, uint32_t bg)
{
  unsigned int i, j, b;
  uint32_t *c_start = (uint32_t *) (lfb_font.psf_font_start + c*lfb_font.char_sz);

  uint32_t *line;
  uint16_t glyph;
  uint32_t mask;

  line = c_start;
  for(j = 0; j<lfb_font.char_h; j++)
  {
    for(b = 0; b<lfb_font.byteperline; b++)
    {
      mask = 1 << ((8 * (b+1)) - 1);
      for(i = 0; i<8; i++)
      {
        if(i+b*8 >= lfb_font.char_w)
          break;
        glyph = *line & mask;
        set_point(x+i+b*8, y+j, (glyph)?fg:bg);
        mask >>= 1;
      }
    }
    line = (uint32_t *) (((uintptr_t) line) + lfb_font.byteperline);
  }
}

void lfb_write(const char *s, uint32_t x, uint32_t y)
{
  unsigned int x_base = x;
  unsigned int i;
  for(i = 0; s[i] != 0; i++)
  {
    if(s[i] == '\n')
    {
      y += lfb_font.char_h;
      x = x_base;
      continue;
    }
    write_char(s[i], x, y, 0xFFFFFFFF, 0xFF000000);
    x += lfb_font.char_w;
  }
}
