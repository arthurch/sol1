#ifndef TYPES_H
#define TYPES_H

#include <stdint.h>

#undef NULL
#define NULL ((void *)0)

typedef uint32_t size_t;
typedef int ssize_t; /* signed size_t */

#endif
