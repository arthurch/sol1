#ifndef VFS_H
#define VFS_H

#include <types.h>

#define PATH_SEPARATOR "/"
#define CURRENT_DIRECTORY "."
#define PARENT_DIRECTORY ".."

/* file functions typedefs */
typedef int(*fs_open_t)(struct fs_node *this, char *filename, int flags);
typedef ssize_t(*fs_write_t)(struct fs_node *this, const void *buf, size_t count);
typedef ssize_t(*fs_read_t)(struct fs_node *this, void *buf, size_t count);
typedef ssize_t(*fs_close_t)(struct fs_node *this);

/* filesystem node (representing a file or something like that) */
struct fs_node {
  char *filename;

  /* functions to handle me */
  fs_open_t open;
  fs_write_t write;
  fs_read_t read;
  fs_close_t close;
};
typedef struct fs_node fs_node_t;

/* filesystem functions */
typedef fs_node_t *(*fs_open_node_t)(char *filename, int flags);
typedef fs_node_t *(*fs_close_node_t)(struct fs_node *node);
/* filesystem structure */
struct fs {
  fs_open_node_t open_node;
  fs_close_node_t close_node;
};
typedef struct fs fs_t;

/* mountpoints */
struct fs_mnt {
  char *mountpoint;

  fs_t *fs;
};
typedef struct fs_mnt fs_mnt_t;

void vfs_init();

void mount_fs(char *path, fs_t *fs);
/* unmount a filesystem.
  care mnt is freed by this function ! */
void unmount_fs(fs_mnt_t *mnt);

fs_node_t *open_fs(char *filename, int flags);
ssize_t close_fs(fs_node_t *node);

#endif
