#ifndef KERNEL_H
#define KERNEL_H

#include <solk/multiboot.h>
#include <solk/mm/kmalloc.h>

#include <solk/sys/mem.h>

#include <types.h>

/*
* memory region entry
* type : 1 -> available
*        others -> reserved (in general)
*/
struct boot_mmap_entry {
  uintptr_t base;
  uint32_t length;
  uint32_t type;
};

/*
* boot_t structure used to pass multiboot informations to kernel
*/
struct boot {
  /* list memory regions (only 32 bits length) */
  struct boot_mmap_entry *memory_map;
  uint32_t memory_map_count;

  /* total accessible memory (in kiB) */
  uint64_t mem_total;

  /* framebuffer informations (framebuffer_addr = 0 if not supported) */
  uint64_t framebuffer_addr;
  uint32_t framebuffer_pitch;
  uint32_t framebuffer_width;
  uint32_t framebuffer_height;
  uint8_t framebuffer_bpp;
#define BOOT_FRAMEBUFFER_TYPE_INDEXED 0
#define BOOT_FRAMEBUFFER_TYPE_RGB     1
#define BOOT_FRAMEBUFFER_TYPE_EGA_TEXT  2
  uint8_t framebuffer_type;
};
typedef struct boot boot_t;

boot_t *process_multiboot_info(uint32_t mb_sig, multiboot_info_t *mbi);
void validate_memory_region(boot_t *b, uint32_t addr, uint32_t length, uint32_t type);

void solk_init();

extern char __kernel_end;
inline uint32_t virtual_kernel_sz()
{
  return (((uint32_t)&__kernel_end + PAGE_MASK) & ~PAGE_MASK) + PMM_STACK_SIZE + KERNEL_EARLY_HEAP_SIZE;
}

inline uint32_t physical_kernel_sz()
{
  return (((uint32_t)&__kernel_end + PAGE_MASK) & ~PAGE_MASK);
}

#endif
