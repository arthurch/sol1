#ifndef PANIC_H
#define PANIC_H

#include <types.h>

extern void _panic_halt_loop(void);

#define PANIC(errmsg) kernel_panic(0, errmsg, __FILE__, __LINE__)
#define PANICE(errcode, errmsg) kernel_panic(errcode, errmsg, __FILE__, __LINE__)

/* not much we can do early on as we can't print to the screen */
#define EARLY_PANIC(errmsg) _panic_halt_loop()

__attribute__((noreturn)) void kernel_panic(uint32_t errcode, const char *errmsg, const char *file, int line);

#endif
