#ifndef TTY_H
#define TTY_H

#include <stdint.h>
#include <stdio.h>

#define TTY_BUFFER_SZ         65536
#define TTY_STDOUT_BUFFER_SZ  4096

#define TTY_DEFAULT_FG  0xFFFFFFFF
#define TTY_DEFAULT_BG  0xFF000000

#define TTY_NEWLINE  0xA
#define TTY_ESC      0x1B

int tty_terminal_init();

int tty_putc(int c);

void tty_auto_scroll();
void tty_flush();
void tty_clear();

/* remove every characters before display seek */
void tty_reset();

void tty_setfg(uint32_t fg);
void tty_setbg(uint32_t bg);

extern _IO_FILE_plus_t _TTY_out_stream;
extern const struct _IO_jump_t IO_TTY_jump;

extern FILE *TTY_stdout;

/* Jumpable functions for tty stream */
extern int _IO_TTY_finish(FILE *stream);
extern int _IO_TTY_overflow(FILE *stream);
extern int _IO_TTY_underflow(FILE *stream);
extern off64_t _IO_TTY_seek(FILE *stream, off64_t off, int mode);
extern int _IO_TTY_read(FILE *stream, void *data, size_t n);
extern int _IO_TTY_write(FILE *stream, const void *data, size_t n);
extern int _IO_TTY_close(FILE *stream);

#endif
