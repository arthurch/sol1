#ifndef VIDEO_H
#define VIDEO_H

#include <solk/kernel.h>
#include <solk/video/psf.h>

#include <types.h>

#define IO_VID_WIDTH       0x1
#define IO_VID_HEIGHT      0x2
#define IO_VID_BPP         0x3
#define IO_VID_PITCH       0x4
#define IO_VID_FB_BASE     0x5
#define IO_VID_NATIVE_FONT 0x6
#define IO_VID_CHAR_W      0x7
#define IO_VID_CHAR_H      0x8

struct psf_font {
  void *psf_font_start;
  uint32_t length;
  uint32_t char_sz;
  uint32_t char_h;
  uint32_t char_w;
  uint32_t byteperline;
};
typedef struct psf_font psf_font_t;

void video_init(boot_t *kb, psf2_header_t *psf_h);

/* Linear framebuffer functions */
void lfb_video_init(boot_t *kb);
void set_point(unsigned int x, unsigned int y, uint32_t c);
void set_point24(unsigned int x, unsigned int y, uint32_t c);
int ioctl_vid(void *unused, unsigned long request, void *argp);
void lfb_clr();
void write_char(unsigned char c, uint32_t x, uint32_t y, uint32_t fg, uint32_t bg);
void lfb_write(const char *s, uint32_t x, uint32_t y);

/* Manage PSF fonts */
int load_psf_font(psf_font_t *font, psf2_header_t *psf_h);

uint32_t rgbtoi(unsigned char r, unsigned char g, unsigned char b);
uint32_t rgbatoi(unsigned char r, unsigned char g, unsigned char b, unsigned char a);

#endif
