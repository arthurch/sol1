#ifndef CPU_H
#define CPU_H

#include <types.h>

#define SOFTWARE_INT_VECTOR (0x80)

void arch_enable_interrupts();
void arch_disable_interrupts();

void arch_cpu_setup();

uint64_t arch_cpu_nanos();

#endif
