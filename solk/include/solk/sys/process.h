#ifndef PROCESS_H
#define PROCESS_H

#include <solk/arch/i386/sys/task.h>

#include <types.h>

#include <list.h>

#define KERNEL_STACK_SIZE 0x4000
#define STACK_ALIGN 16
#define SYS_MAX_PID 32768

typedef signed int pid_t;
typedef signed int tid_t;

struct thread {
  tid_t tid;
  char *name;

  task_t task;
  uintptr_t stack;
  uintptr_t user_stack;

  struct process *owner;
};
typedef struct thread thread_t;

struct process {
  pid_t pid;
  char *name;

  struct process *parent;

  /* this is single threaded for the moment */
  thread_t thread;

  uint8_t running;
#define PROC_NOT_HANDLED 0x0
#define PROC_READY 0x1
#define PROC_PAUSED 0x2
  uint8_t state;
  uint64_t wait_until;

  /* process' node in ready_list */
  node_t *ready_node;
  /* process' node in waiting_list or sleeping_list */
  node_t *paused_node;
};
typedef struct process process_t;

void tasking_init(uint32_t scheduler_freq);
void spawn_init();

pid_t getpid(process_t *proc);

process_t *getprocess(pid_t pid);

void spawn_thread(thread_t *th, uintptr_t entry_point, char *name);
process_t *spawn_process(process_t *parent, uintptr_t entry_point, char *name, uint8_t ready);
void make_process_ready(process_t *proc);

void destroy_process(process_t *proc);

void process_sleep(uint64_t millis);
void pause_process(process_t *proc, uint64_t sleep);
void resume_process(process_t *proc);
void awake_sleepers(uint64_t current_time);

void switch_next();

/*** TEMP ***/
void task_1();
void task_2();
void task_3();
/************/

#endif
