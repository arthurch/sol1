#ifndef MEM_M
#define MEM_H

#include <types.h>

/* 4 KiB pages */
#define PAGESIZE (4096)
#define PAGE_MASK (PAGESIZE-1)

/* x86 table size */
#define TABLESIZE (1024 * PAGESIZE)
#define TABLE_MASK (TABLESIZE-1)

#define PAGE_PRESENT (1)
#define PAGE_RW (1<<1)

/* graphic framebuffer virtual address */
#define FB_VIRTUAL_ADDR (0xF0000000)

/* pmm mapped stack size (one page) */
#define PMM_STACK_SIZE (4096)

/* macros to switch between higher/lower half kernel addresses */
extern char __kernel_vma; /* defined in linker script */
#define VMA(obj) ((typeof((obj)))((uintptr_t)(void*)&__kernel_vma + (uintptr_t)(void*)(obj)))
#define LMA(obj) ((typeof((obj)))((uintptr_t)(void*)(obj)) - (uintptr_t)(void*)&__kernel_vma)

#define KERNEL_VMA ((uintptr_t)&__kernel_vma)

/* arch-dependant functions to manipulate pages (defined in arch/.../mem/) */

uintptr_t arch_get_physaddr(uintptr_t vaddr);
/* map a page to given virtual address */
uintptr_t arch_map_page(uintptr_t paddr, uintptr_t vaddr, uint32_t flags);
/* same but don't allocate a new table if needed
  (return 0 on failure) */
uintptr_t arch_safe_map_page(uintptr_t paddr, uintptr_t vaddr, uint32_t flags);
void arch_unmap_page(uintptr_t vaddr);

#endif
