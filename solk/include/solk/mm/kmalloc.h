#ifndef KMALLOC_H
#define KMALLOC_H

#include <types.h>
#include <solk/sys/mem.h>

#define KERNEL_EARLY_HEAP_SIZE 8192

#define KERNEL_DEFAULT_HEAP_SIZE TABLESIZE

/**** dumb allocation algorithm (can't free memory) ****/

void set_early_heap(uintptr_t heap);
void *early_kmalloc(size_t sz);
/* care using this as it free whole early heap */
void early_freeall();

/**** smart allocation algorithm is in libk ****/

/* initialize kernel heap at address kernel_heap */
void kmalloc_init(uintptr_t kernel_heap);

/* allocate and free aligned memory */
void *kvalloc(size_t sz, size_t align);
void kvfree(void *ptr);

#endif
