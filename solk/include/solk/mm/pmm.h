#ifndef PMM_H
#define PMM_H

/*
*
* Physical Memory Manager (using a stack based algorithm)
*  free pages are stored in stacks one page wide.
*  only one stack is mapped at a time
*
*/

#include <solk/kernel.h>

#include <solk/sys/mem.h>

#include <stdint.h>

#define PMM_PTR_SIZE 4
#define PMM_STACK_LENGTH 1023

/* structure representing a stack of free pages
  next_stack store the address of the next stack
  to be mapped if this stack is empty. This structure
  has a size of exactly 4096 B (one page)
  (assuming uintptr_t is 32 bit wide) */
struct pmm_stack {
  uint32_t pages[PMM_STACK_LENGTH];
  uintptr_t next_stack;
};
typedef struct pmm_stack pmm_stack_t;

/* allocate a physical page
  return : *physical* address of allocated page on success
    , 0 on failure */
uintptr_t pmm_allocate_page();
/* free a physical page
  param : *physical* address of page to be freed */
void pmm_free_page(uintptr_t page);

/* initialize pmm
  return : 0 on success, negative value on failure */
void pmm_init(boot_t *kb);
void parse_available_region(uint32_t base, uint32_t length);
/* push a free page */
void push_free_page(uint32_t paddr);
/* pop a free page, return 0 on failure */
uint32_t pop_free_page();
/* get how many free pages are stored in the stacks */
uint32_t pmm_free_pages_count();
uint32_t pmm_usable_mem();
uintptr_t pmm_current_stack();

#endif
