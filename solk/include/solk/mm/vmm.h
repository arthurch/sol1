#ifndef VMM_H
#define VMM_H

#include <types.h>

/* pagemap_t represent pages in virtual memory */
struct pagemap {
#define PAGEMAP_NOT_USED 0         /* pagemap not used */
#define PAGEMAP_NOT_INITIALIZED 0  /* pagemap not initialized yet (same behaviour as PAGEMAP_NOT_USED) */
#define PAGEMAP_MAPPED 1           /* pagemap representing real physical pages mapped at a virtual address */
#define PAGEMAP_ALLOCATED 2        /* pagemap allocated using random physical pages */
  int status;
  uint32_t size;
  uint32_t pagecount;
  uintptr_t base;
  uintptr_t *pages;
};
typedef struct pagemap pagemap_t;

/* a pointer to the arch dependant paging structure */
typedef uintptr_t arch_vmspace;

/** allocation utilities **/

/* allocate and free RW pages at specific address */
uintptr_t vmm_alloc_page_at(uintptr_t vaddr, int flags);
void vmm_free_page_at(uintptr_t vaddr);

/** pagemap utilities **/

/* create a page map (using early malloc) */
pagemap_t *create_pagemap_early(pagemap_t *pm, uint32_t base, uint32_t size);
/* create a page map */
pagemap_t *create_pagemap(pagemap_t *pm, uintptr_t base, uint32_t size);

/* allocate pagemap using random physical pages from pmm */
pagemap_t *allocate_pagemap_at(pagemap_t *pm, uint32_t base, uint32_t size, int flags);
void allocate_pagemap(pagemap_t *pm, int flags);
/* free all the pages from a pagemap (using pmm) */
void free_pagemap(pagemap_t *pm);

void map_pagemap(pagemap_t *pm, int flags);
void map_pagemap_at(pagemap_t *pm, uintptr_t base, int flags);
void unmap_pagemap(pagemap_t *pm);

/** manage whole virtual memory space (done by arch dependant code) **/

arch_vmspace arch_clone_kernel_vmspace();
int arch_switch_vmspace(arch_vmspace vmspace);

/** initialize virtual memory manager **/

void vmm_init();
void arch_vmm_init();

/** page fault handler **/

void vmm_page_fault(uint32_t error, uintptr_t vaddr, uintptr_t ret_addr);

#endif
