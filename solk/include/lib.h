#ifndef LIB_H
#define LIB_H

#include <types.h>

typedef unsigned long int op_t;
#define OPSIZ (sizeof(op_t))

void *memset(void *ptr, int val, size_t sz);
void *memcpy(void *__restrict dptr, const void *__restrict sptr, size_t sz);
void *memmove(void *dstptr, const void *srcptr, size_t size);

#endif
