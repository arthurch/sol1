PROJECT:=solk

### INCLUDE DIR ###
KER_INCLUDE_DIR:=include

### DEFAULT FLAGS ###
CFLAGS?=-O2 -g
LFLAGS?=-O2

### BUILDING FLAGS ###
CFLAGS:=$(CFLAGS) -std=gnu11 -ffreestanding -Wall -Wextra
LFLAGS:=$(LFLAGS) -ffreestanding -nostdlib
LIBS:=$(LIBS) -lgcc -lk

### BUILDING MACROS ###
CFLAGS:=$(CFLAGS) -D __is_kernel

### SUB DIRS ###
ARCHDIR:=arch/$(ARCH)
KDATADIR:=kdata
MAKE_CONFIG_FILE=config.makefile
MAKE_INCLUDES:=$(ARCHDIR)/$(MAKE_CONFIG_FILE) $(KDATADIR)/$(MAKE_CONFIG_FILE)

include $(MAKE_INCLUDES)

ARCH_INCLUDE_DIR_SYS:=$(INCLUDE_DIR_SYS)/$(PROJECT)/$(ARCHDIR)

### OBJS TO BE BUILD ###
KERNEL_OBJS:=\
	$(KERNEL_ARCH_OBJS) \
	lib.o \
	kernel/kernel.o \
	kernel/multiboot.o \
	mm/pmm.o mm/vmm.o mm/kmalloc.o \
	sys/process.o \
	video/video.o video/lfbvideo.o \
	tty/tty.o \
	fs/vfs.o

### GLOBAL CONSTRUCTORS OBJS
CRTI_OBJ=$(ARCHDIR)/crti.o
CRTBEGIN_OBJ:=$(shell $(CC) $(CFLAGS) $(LDFLAGS) -print-file-name=crtbegin.o)
CRTEND_OBJ:=$(shell $(CC) $(CFLAGS) $(LDFLAGS) -print-file-name=crtend.o)
CRTN_OBJ=$(ARCHDIR)/crtn.o

### FINAL LINKING LIST ###
OBJS:=\
	$(KERNEL_OBJS) \
	$(ARCH_OBJS)
LINKING_LIST:=$(CRTI_OBJ) $(CRTBEGIN_OBJ) $(OBJS) $(KDATA_OBJS) $(CRTEND_OBJ) $(CRTN_OBJ)
LINKING_FILE:=$(ARCHDIR)/linker.ld

### OUTPUT FILE ###
OUTPUT?=sol1.ker

### BUILDING COMMANDS ###
.PHONY: all install install-headers clean cleanall

all: $(OUTPUT)

install: $(OUTPUT)
	@$(ECHON) installing kernel to $(BOOT_DIR)...
	@mkdir -p $(BOOT_DIR)
	@$(CP) $(OUTPUT) $(BOOT_DIR)/$(OUTPUT)
	@echo done

install-headers:
	@$(ECHON) installing headers from kernel to $(INCLUDE_DIR)...
	@mkdir -p $(INCLUDE_DIR_SYS)
	@$(CP) -R $(KER_INCLUDE_DIR)/. $(INCLUDE_DIR_SYS)/.
	@mkdir -p $(ARCH_INCLUDE_DIR_SYS)
	@$(CP) -R $(ARCH_INCLUDE_DIR)/. $(ARCH_INCLUDE_DIR_SYS)/.
	@echo done

### FILES TO BE BUILD ###
$(OUTPUT): $(OBJS) $(CRTI_OBJ) $(CRTN_OBJ) $(LINKING_FILE)
	$(CC) -T $(LINKING_FILE) -o $@ $(LFLAGS) $(LINKING_LIST) $(LIBS)

%.o: %.c Makefile
	$(CC) -MD $(CFLAGS) -c $< -o $@

%.o: %.S Makefile
	$(AS) -c $< -o $@

### CLEANING ###
clean:
	@$(ECHON) cleaning kernel...
	@$(RM) -f $(OBJS)
	@$(RM) -f $(CRTI_OBJ) $(CRTN_OBJ)
	@$(RM) -f $(OBJS:.o=.d)
	@echo done

cleanall: clean
	@$(ECHON) cleaning built binaries...
	@$(RM) -f $(OUTPUT)
	@echo done

-include $(OBJS:.o=.d)
