#include <solk/mm/pmm.h>

#include <solk/panic.h>

#include <stdio.h>

#include <lib.h>

/* kernel end symbol */
extern char __kernel_end;

/* currently mapped stack */
static pmm_stack_t *current_stack;
/* stack pointer */
static uintptr_t stack_ptr;
/* free pages counter */
static uint32_t free_pages_counter;
static uint32_t usable_mem;

void pmm_init(boot_t *kb)
{
  /* current stack is mapped at the end of the higher half kernel (in virtual memory) */
  current_stack = (pmm_stack_t *) VMA(physical_kernel_sz());
  stack_ptr = 0;

  usable_mem = 0;
  unsigned int i = 0;
  for(i = 0; i<kb->memory_map_count; i++)
  {
    struct boot_mmap_entry *e = &kb->memory_map[i];
    if(e->type == 1)
    {
      /* Available memory */
      parse_available_region(e->base, e->length);
    }
  }
}

void parse_available_region(uint32_t base, uint32_t length)
{
  /* page-aligned kernel end address */
  uint32_t kend = physical_kernel_sz();
  if(base < kend)
  {
    if(length > (kend - base))
    {
      /* this region is partially used by kernel */
      length -= (kend - base);
      base = kend;
    } else
    {
      /* this region is used by kernel */
      return;
    }
  }
  /* make sure base is page aligned */
  base = base & ~PAGE_MASK;

  /* add pages to stack */
  unsigned int i;
  unsigned int pages = length/PAGESIZE;
  uint32_t p = base;
  for(i = 0; i<pages; i++)
  {
    push_free_page(p);
    p += PAGESIZE;
    usable_mem += PAGESIZE;
  }
}

static void pmm_map_stack(uintptr_t paddr)
{
  if(arch_safe_map_page(paddr, (uintptr_t) current_stack, PAGE_RW) == 0)
  {
    EARLY_PANIC("pmm: can't map stack");
  }
}

void push_free_page(uint32_t paddr)
{
  if(stack_ptr == 0)
  {
    /* no stack is available, this page will be our new stack */
    pmm_map_stack(paddr);
    memset((void *) current_stack, 0, PMM_STACK_SIZE);

    stack_ptr = (uintptr_t) current_stack->pages;
  } else if(stack_ptr >= ((uintptr_t) current_stack + PMM_PTR_SIZE*PMM_STACK_LENGTH))
  {
    /* current stack is full, this page will be our new stack */
    uintptr_t last_paddr = arch_get_physaddr((uintptr_t) current_stack);
    pmm_map_stack(paddr);
    memset((void *) current_stack, 0, PMM_STACK_SIZE);

    stack_ptr = (uintptr_t) current_stack->pages;
    current_stack->next_stack = last_paddr;
  } else
  {
    *((uint32_t *) stack_ptr) = paddr;
    stack_ptr += PMM_PTR_SIZE;
    free_pages_counter++;
  }
}

uint32_t pop_free_page()
{
  if(stack_ptr == 0)
  {
    /* no stacks are available */
    return 0;
  } else if(stack_ptr == ((uintptr_t) current_stack))
  {
    /* current stack is empty or no pages left in it, map the next stack */
    uintptr_t next_stack = current_stack->next_stack;
    if(next_stack == 0)
    {
      /* no more stacks are available (no more free memory) */
      return 0;
    }
    uintptr_t old_stack = arch_get_physaddr((uintptr_t) current_stack);

    pmm_map_stack(next_stack);

    uint32_t free = current_stack->pages[1022];
    current_stack->pages[1022] = old_stack;

    stack_ptr = (uintptr_t) &(current_stack->pages[1022]);

    return free;
  } else
  {
    stack_ptr -= PMM_PTR_SIZE;
    uint32_t free = *((uint32_t *) stack_ptr);
    free_pages_counter--;

    return free;
  }
}

uintptr_t pmm_allocate_page()
{
  return pop_free_page();
}

void pmm_free_page(uintptr_t page)
{
  push_free_page(page);
}

uintptr_t pmm_current_stack()
{
  return (uintptr_t) current_stack;
}

uint32_t pmm_free_pages_count()
{
  return free_pages_counter;
}

uint32_t pmm_usable_mem()
{
  return usable_mem;
}
