#include <solk/mm/kmalloc.h>

#include <malloc.h>

#include <solk/mm/vmm.h>
#include <solk/sys/mem.h>
#include <solk/kernel.h>
#include <solk/panic.h>

#include <types.h>
#include <lib.h>

#include <stdio.h>

/** smart allocation algorithm **/

static pagemap_t kernel_heap_pm;

void kmalloc_init(uintptr_t kernel_heap)
{
  /* map kernel heap to virtual memory */
  create_pagemap_early(&kernel_heap_pm, kernel_heap, KERNEL_DEFAULT_HEAP_SIZE);
  allocate_pagemap(&kernel_heap_pm, PAGE_RW);

  /* zeroing out heap */
  memset((void *) kernel_heap, 0, KERNEL_DEFAULT_HEAP_SIZE);

  /* initialize acmalloc algorithm */
  acmalloc_init(kernel_heap, KERNEL_DEFAULT_HEAP_SIZE);
}

size_t kernel_heap_expand(size_t sz)
{
  /* minimum size is TABLESIZE */
  sz = (sz + TABLE_MASK) & ~TABLE_MASK;

  uintptr_t kernel_heap_end = kernel_heap_pm.base + kernel_heap_pm.size;

  /* allocate new memory */
  pagemap_t heap_expand_pm;
  allocate_pagemap_at(&heap_expand_pm, kernel_heap_end, sz, PAGE_RW);

  /* zeroing out everything */
  memset((void *) kernel_heap_end, 0, sz);

  /* expanding heap size */
  kernel_heap_pm.size += sz;

  return sz;
}

/*
 * Allocate aligned memory
 * memory allocated using kvalloc needs to be free
 * using kvfree
 * THIS IS HORRIBLY WASTEFUL USE IT ONLY IF NECESSARY
 *
 * @return pointer to allocated memory
 */
void *kvalloc(size_t sz, size_t align)
{
  size_t true_sz = sz + (align-1) + sizeof(uintptr_t);
  uintptr_t ptr = (uintptr_t) malloc(true_sz);
  uintptr_t aligned = (ptr + (align-1) + sizeof(uintptr_t)) & (~(align-1));

  *((uintptr_t *) (aligned - sizeof(uintptr_t))) = ptr;

  if(aligned % align != 0)
  {
    free((void *) ptr);
    return NULL;
  }
  return (void *) aligned;
}

/*
 * Free memory allocated using kvalloc
 */
void kvfree(void *ptr)
{
  if(__builtin_expect(ptr == NULL, 0))
  {
    return;
  }
  uintptr_t true_ptr = *((uintptr_t *) (((uintptr_t) ptr) - sizeof(uintptr_t)));
  free((void *) true_ptr);
}

/** dumb allocation algorithm **/

static uintptr_t early_heap;
static uintptr_t early_heap_ptr;

void set_early_heap(uintptr_t heap)
{
  early_heap = heap;
  early_heap_ptr = heap;
}

void *early_kmalloc(size_t sz)
{
  if(sz > (KERNEL_EARLY_HEAP_SIZE - (early_heap_ptr - early_heap)))
  {
    /* no more space (early heap is not extensible) */
    PANIC("early heap overflow");
    return 0;
  }

  void *ptr = (void *) early_heap_ptr;
  early_heap_ptr += sz;
  return ptr;
}

void early_freeall()
{
  /* use it at your own risk ! */
  early_heap_ptr = early_heap;
}
