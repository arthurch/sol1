#include <solk/mm/vmm.h>

#include <solk/mm/pmm.h>
#include <solk/sys/mem.h>

#include <stdio.h>
#include <malloc.h>

#include <types.h>

/** allocation utilities **/

/* TODO : allocate when first accessed */
uintptr_t vmm_alloc_page_at(uintptr_t vaddr, int flags)
{
  uintptr_t p = pmm_allocate_page();
  if(p == 0)
    return 0;
  return arch_map_page(p, vaddr, PAGE_PRESENT | flags);
}

void vmm_free_page_at(uintptr_t vaddr)
{
  uintptr_t p = arch_get_physaddr(vaddr);
  if(p == 0)
    return;
  arch_unmap_page(vaddr);
  pmm_free_page(p);
}

pagemap_t *create_pagemap_early(pagemap_t *pm, uint32_t base, uint32_t size)
{
  if(pm == NULL)
  {
    pm = (pagemap_t *) early_kmalloc(sizeof(pagemap_t));
  }

  pm->status = PAGEMAP_NOT_USED;

  pm->pages = 0;
  pm->pagecount = (size + PAGE_MASK) / PAGESIZE;
  pm->base = base;
  pm->size = size;

  return pm;
}

pagemap_t *create_pagemap(pagemap_t *pm, uint32_t base, uint32_t size)
{
  if(pm == NULL)
  {
    pm = (pagemap_t *) malloc(sizeof(pagemap_t));
  }

  pm->status = PAGEMAP_NOT_USED;

  pm->pages = 0;
  pm->pagecount = (size + PAGE_MASK) / PAGESIZE;
  pm->base = base;
  pm->size = size;

  return pm;
}

void allocate_pagemap(pagemap_t *pm, int flags)
{
  if(pm == NULL)
  {
    return;
  }

  if(pm->status == PAGEMAP_ALLOCATED)
  {
    free_pagemap(pm);
  } else if(pm->status == PAGEMAP_MAPPED)
  {
    unmap_pagemap(pm);
  }

  for(size_t i = 0; i<pm->pagecount; i++)
  {
    vmm_alloc_page_at(pm->base + i*PAGESIZE, flags);
  }

  pm->pages = 0;
  pm->status = PAGEMAP_ALLOCATED;
}

pagemap_t *allocate_pagemap_at(pagemap_t *pm, uint32_t base, uint32_t size, int flags)
{
  if(pm == NULL)
  {
    pm = create_pagemap(pm, base, size);
  }

  pm->pagecount = (size + PAGE_MASK) / PAGESIZE;
  pm->base = base;
  pm->size = size;

  allocate_pagemap(pm, flags);

  return pm;
}

void free_pagemap(pagemap_t *pm)
{
  if(pm == NULL || pm->status != PAGEMAP_ALLOCATED)
  {
    return;
  }

  for(size_t i = 0; i<pm->pagecount; i++)
  {
    vmm_free_page_at(pm->base + i*PAGESIZE);
  }

  pm->status = PAGEMAP_NOT_USED;
}

void map_pagemap(pagemap_t *pm, int flags)
{
  map_pagemap_at(pm, pm->base, flags);
}

void map_pagemap_at(pagemap_t *pm, uintptr_t base, int flags)
{
  if(pm->status == PAGEMAP_MAPPED)
  {
    unmap_pagemap(pm);
  }

  if(pm->pages == 0)
  {
    return;
  }

  pm->base = base;
  for(size_t i = 0; i<pm->pagecount; i++)
  {
    if(pm->pages[i] == 0)
    {
      arch_unmap_page(pm->base + i*PAGESIZE);
    } else
    {
      arch_map_page(pm->pages[i], pm->base + i*PAGESIZE, flags);
    }
  }
  pm->status = PAGEMAP_MAPPED;
}

void unmap_pagemap(pagemap_t *pm)
{
  if(pm->status != PAGEMAP_MAPPED)
    return;
  for(size_t i = 0; i<pm->pagecount; i++)
  {
    if(pm->pages[i] == 0)
    {
      continue;
    } else
    {
      arch_unmap_page(pm->base + i*PAGESIZE);
    }
  }

  pm->status = PAGEMAP_NOT_USED;
}

void vmm_init()
{
  arch_vmm_init();
}

extern void _panic_halt_loop();
void vmm_page_fault(uint32_t error, uintptr_t vaddr, uintptr_t ret_addr)
{
  printf("Page fault (err=%b, vaddr=0x%X, ret=%p)\n", error, vaddr, ret_addr);
  fflush(stdout);
  _panic_halt_loop();
}
