#include <solk/tty/tty.h>

#include <solk/panic.h>

#include <solk/video/video.h>

#include <malloc.h>
#include <string.h>

/* IO vtable for our tty display */
const struct _IO_jump_t IO_TTY_jump = {
  _IO_TTY_finish,
  _IO_TTY_overflow,
  _IO_TTY_underflow,
  _IO_TTY_seek,
  _IO_TTY_read,
  _IO_TTY_write,
  _IO_TTY_close
};

/* IO wrapper for our tty display */
_IO_FILE_plus_t _TTY_out_stream;
FILE *TTY_stdout = &_TTY_out_stream.file;
/* temporary buffer (which will be eventually flushed out to tty's buffer) */
char TTY_stdout_buffer[TTY_STDOUT_BUFFER_SZ];

 /* actual tty buffer containing characters to display */
unsigned char tty_buffer[TTY_BUFFER_SZ];
uint32_t tty_seek = 0; /* current writing position (in tty_buffer) */

/* things about displaying characters to screen */
uint32_t tty_disp_end = 0; /* we can stop displaying at this buffer index */
uint32_t tty_disp_seek = 0; /* we can start displaying at this buffer index */
uint32_t tty_def_foreground = TTY_DEFAULT_FG;
uint32_t tty_def_background = TTY_DEFAULT_BG;
/* character size */
uint32_t tty_char_w;
uint32_t tty_char_h;

int tty_terminal_init()
{
  tty_seek = 0;
  tty_disp_end = 0;
  tty_disp_seek = 0;

  ioctl_vid(NULL, IO_VID_CHAR_W, &tty_char_w);
  ioctl_vid(NULL, IO_VID_CHAR_H, &tty_char_h);

  memset(&tty_buffer, 0, TTY_BUFFER_SZ);

  TTY_stdout = &_TTY_out_stream.file;

  io_stream_init(TTY_stdout, _IO_NO_READS | _IO_USER_BUF);
  _TTY_out_stream.vtable = &IO_TTY_jump;

  int ret = setvbuf(TTY_stdout, TTY_stdout_buffer, _IOLBF, TTY_STDOUT_BUFFER_SZ);
  if(ret != 0)
  {
    return ret;
  }

  return 0;
}

/* remove every characters before tty_disp_seek */
void tty_reset()
{
  tty_auto_scroll();

  unsigned char *saved = (unsigned char *) malloc(tty_disp_end - tty_disp_seek + 1);
  /* move displayed characters to the beginning of the buffer */
  memcpy(saved, (void *) ((uintptr_t) tty_buffer) + tty_disp_seek, tty_disp_end - tty_disp_seek + 1);
  memcpy(tty_buffer, saved, tty_disp_end - tty_disp_seek + 1);
  /* reset seek pointers */
  tty_disp_end -= tty_disp_seek;
  tty_seek -= tty_disp_seek;
  tty_disp_seek = 0;
}

int tty_putc(int c)
{
  unsigned char cc = (unsigned char) c;

  if(tty_seek >= TTY_BUFFER_SZ)
  {
    /* buffer is filled up, trying to reset it */
    tty_reset();

    if(tty_seek >= TTY_BUFFER_SZ)
    {
      /* well... panic */
      __set_errno(ENOMEM);
      PANIC("tty display - buffer overflow");
      return -1;
    }
  }
  tty_buffer[tty_seek] = cc;

  tty_seek++;
  if(tty_seek>tty_disp_end) /* if we wrote a character above display end, update it */
    tty_disp_end = tty_seek;

  return cc;
}

void tty_clear()
{
  /* shall we remove everything ? idk */
  tty_disp_end = 0;
  tty_disp_seek = 0;
  tty_seek = 0;
  lfb_clr();
}

/* scroll one line down */
static void tty_scrolldown(uint16_t w)
{
  unsigned int x = 0;
  unsigned int i = 0;
  for(i = tty_disp_seek; i<tty_disp_end; i++)
  {
    if(tty_buffer[i] == TTY_NEWLINE)
    {
      tty_disp_seek = i+1;
      return;
    }
    if(x + tty_char_w >= w)
    {
      tty_disp_seek = i;
      return;
    }
    x += tty_char_w;
  }
}

void tty_auto_scroll()
{
  uint16_t w, h;
  ioctl_vid(NULL, IO_VID_WIDTH, &w);
  ioctl_vid(NULL, IO_VID_HEIGHT, &h);

  /* calculate buffer size when displayed (starting at disp_seek) */
  unsigned int total_height = tty_char_h;
  unsigned int x = 0;
  unsigned int i;
  for(i = tty_disp_seek; i<tty_disp_end; i++)
  {
    if(tty_buffer[i] == TTY_NEWLINE)
    {
      total_height += tty_char_h;
      x = 0;
    }
    if(x + tty_char_w >= w)
    {
      x = 0;
      total_height += tty_char_h;
    }
    x += tty_char_w;
  }

  /* if it's to high, scroll as many times as needed */
  if(total_height >= h)
  {
    /* screen is too small, scroll */
    unsigned int extra_lines = (total_height - h + tty_char_h) / tty_char_h;
    for(i = 0; i<extra_lines; i++)
    {
      tty_scrolldown(w);
    }
  }
}

/* flush tty's buffer to the screen */
void tty_flush()
{
  unsigned char c;
  unsigned int x = 0;
  unsigned int y = 0;

  uint16_t w, h;
  ioctl_vid(NULL, IO_VID_WIDTH, &w);
  ioctl_vid(NULL, IO_VID_HEIGHT, &h);

  lfb_clr();
  tty_auto_scroll();

  unsigned int i;
  for(i = tty_disp_seek; i<tty_disp_end; i++)
  {
    c = tty_buffer[i];

    if(c == TTY_NEWLINE)
    {
      x = 0;
      y += tty_char_h;
      continue;
    } else if(c == TTY_ESC)
    {
      /* TODO: parse escape codes */
      continue;
    }

    if(x + tty_char_w >= w)
    {
      x = 0;
      y += tty_char_h;
    }

    if(y+tty_char_h >= h)
    {
      /* shouldn't happen but who knows... */
      return;
    }

    write_char(c, x, y, tty_def_foreground, tty_def_background);
    x += tty_char_w;
  }
}

/* TTY vtable functions */
int _IO_TTY_finish(FILE *stream)
{
  CHECK_FILE(stream, EOF);

  if(stream->_IO_write_ptr != NULL && stream->_IO_write_ptr > stream->_IO_write_base)
  {
    if(IO_OVERFLOW(stream) == EOF)
      return EOF;
  }

  return 0;
}

int _IO_TTY_overflow(FILE *stream)
{
  CHECK_FILE(stream, EOF);

  if(stream->_IO_write_ptr != NULL && stream->_IO_write_ptr > stream->_IO_write_base)
  {
    if(IO_WRITE(stream, (void *) stream->_IO_write_base, (size_t) (stream->_IO_write_ptr - stream->_IO_write_base)) == EOF)
      return EOF;
  }

  tty_flush();

  return 0;
}

// TODO : support stdin
int _IO_TTY_underflow(FILE *stream)
{
  (void) stream;
  return 0;
}

// TODO : support seek functions
off64_t _IO_TTY_seek(FILE *stream, off64_t off, int mode)
{
  (void) stream; (void) off; (void) mode;
  return 0;
}

// TODO : support stdin
int _IO_TTY_read(FILE *stream, void *data, size_t n)
{
  (void) stream; (void) data; (void) n;
  return 0;
}

int _IO_TTY_write(FILE *stream, const void *data, size_t n)
{
  (void) stream;

  unsigned int i;
  char *ptr = (char *) data;
  for(i = 0; i<n; i++)
  {
    if(tty_putc((int) *ptr) == -1)
      return EOF;
    ptr++;
  }

  return i;
}

int _IO_TTY_close(FILE *stream)
{
  return IO_FINISH(stream);
}
