#include <solk/fs/vfs.h>

#include <tree.h>
#include <malloc.h>
#include <string.h>

static tree_t *mountpoints;

void vfs_init()
{
  mountpoints = tree_create();

  /* eventually mount everything needed */
}

void mount_fs(char *path, fs_t *fs)
{
  /* check if mountpoint already exist at this address */
  /* search nearest mountpoint already existing */
  /* add mountpoint as a tree node */

  /* TEMP */
  strcpy(path, "/");
  path[1] = 0;

  fs_mnt_t *mountpoint = (fs_mnt_t *) malloc(sizeof(struct fs_mnt));
  mountpoint->mountpoint = (char *) malloc(strlen(path));
  strcpy(mountpoint->mountpoint, path);
  mountpoint->fs = fs;

  tree_root(mountpoints, mountpoint);
}

void unmount_fs(fs_mnt_t *mnt)
{
  /* check if mnt is in mountpoints tree */

  tree_node_t *node = tree_find(mountpoints, mnt);
  if(node == NULL)
    return;
  /* unmount every sub mountpoints */
  tree_node_t *sub;
  while((sub = tree_iterate_children(node)) != NULL)
  {
    unmount_fs((fs_mnt_t *) sub->value);
  }
  tree_remove(node);
  free(mnt->mountpoint);
  free(mnt);
}

fs_node_t *open_fs(char *filename, int flags);
ssize_t close_fs(fs_node_t *node);
