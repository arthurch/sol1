#include <solk/multiboot.h>

#include <solk/kernel.h>
#include <solk/panic.h>
#include <solk/sys/mem.h>

#include <types.h>

boot_t __multiboot_kboot;

boot_t *process_multiboot_info(uint32_t mb_sig, multiboot_info_t *mbi)
{
  /* bootloader *must* be multiboot-compliant */
  if(mb_sig != MULTIBOOT_BOOTLOADER_MAGIC)
  {
    EARLY_PANIC("unknown bootloader");
    return NULL;
  }

  boot_t *b = &__multiboot_kboot;

  if(MULTIBOOT_CHECK_FLAG(mbi->flags, 12))
  {
    b->framebuffer_addr = mbi->framebuffer_addr;
    b->framebuffer_pitch = mbi->framebuffer_pitch;
    b->framebuffer_width = mbi->framebuffer_width;
    b->framebuffer_height = mbi->framebuffer_height;
    b->framebuffer_bpp = mbi->framebuffer_bpp;
    b->framebuffer_type = mbi->framebuffer_type;
  } else
  {
    b->framebuffer_addr = 0;
  }

  if(MULTIBOOT_CHECK_FLAG(mbi->flags, 0))
  {
    b->mem_total = mbi->mem_lower + mbi->mem_upper;
  }

  if(MULTIBOOT_CHECK_FLAG(mbi->flags, 6))
  {
    b->memory_map_count = 0;

    uintptr_t mmap = (uintptr_t) VMA(mbi->mmap_addr);

    uint32_t s = 0;

    multiboot_memory_map_t *entry = (multiboot_memory_map_t *) mmap;
    size_t entries = 0;

    /* we first need to loop throught the table to know how many
    entries we have (because entries are not fixed in size) */
    while(s < mbi->mmap_length)
    {
      entry = (multiboot_memory_map_t *) (mmap + s);
      if(entry->addr_high != 0 || entry->length_high != 0)
      {
        /* we can handle only 32 bits addresses */
        s += entry->size + sizeof(entry->size);
        continue;
      }
      entries++;
      s += entry->size + sizeof(entry->size);
    }
    b->memory_map = (struct boot_mmap_entry *) early_kmalloc(entries * sizeof(struct boot_mmap_entry));

    /* then we can fill our memory map */
    s = 0;
    while(s < mbi->mmap_length)
    {
      entry = (multiboot_memory_map_t *) (mmap + s);
      if(entry->addr_high != 0 || entry->length_high != 0)
      {
        /* we can handle only 32 bits addresses */
        s += entry->size + sizeof(entry->size);
        continue;
      }
      uint32_t addr = entry->addr_low;
      uint32_t length = entry->length_low;
      uint32_t type = entry->type;

      validate_memory_region(b, addr, length, type);
      s += entry->size + sizeof(entry->size);
    }
  }

  return b;
}

void validate_memory_region(boot_t *b, uint32_t addr, uint32_t length, uint32_t type)
{
  uint32_t available = type;
  if(addr < 0x100000)
  {
    available = 0;
  }

  struct boot_mmap_entry *e = &(b->memory_map[b->memory_map_count]);
  e->base = addr;
  e->length = length;
  e->type = available;

  b->memory_map_count++;
}
