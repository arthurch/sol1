#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <malloc.h>
#include <tree.h>

#include <types.h>

#include <solk/kernel.h>
#include <solk/multiboot.h>

#include <solk/video/psf.h>
#include <solk/video/video.h>

#include <solk/tty/tty.h>
#include <solk/mm/pmm.h>
#include <solk/mm/vmm.h>
#include <solk/mm/kmalloc.h>
#include <solk/sys/cpu.h>
#include <solk/sys/mem.h>
#include <solk/sys/process.h>
#include <solk/panic.h>
#include <solk/fs/vfs.h>

#include <solk/arch/i386/sys/task.h>
#include <solk/arch/i386/cpu/cpu.h>

/* check for cross compiling */
#if defined(__linux__)
#error "sol1 needs to be compiled using a cross compiler"
#endif

#if !defined(__i386__)
#error "sol1 needs to be compiled using a x86 compiler"
#endif

/* extern symbols */
extern void _panic_halt_loop();

extern char _binary_font_psf_start;
extern char _binary_font_psf_end;

extern uint32_t sys_multiboot_info;
extern uint32_t sys_multiboot_sig;

extern uint32_t sys_x87_supported;

int errno = 0;

void random_task()
{
  while(1)
  {
    printf("hello %d\n", getpid(NULL));
    fflush(stdout);
    process_sleep(500);
  }
}

void kernel_main(void)
{
  /* set early heap pointer to the end of the pmm stack */
  set_early_heap(VMA(physical_kernel_sz() + PMM_STACK_SIZE));

  /* set up cpu environment */
  arch_cpu_setup();

  multiboot_info_t *mbi = (multiboot_info_t *) &sys_multiboot_info;
  boot_t *kboot = process_multiboot_info(sys_multiboot_sig, mbi);

  /* set up memory management system */
  pmm_init(kboot); /* stage 1 : physical mm */
  vmm_init(); /* stage 2 : virtual mm */
  kmalloc_init((uintptr_t) VMA(virtual_kernel_sz())); /* stage 3 : high level mm */

  unsigned int i, j;

  /* set up linear framebuffer */
  video_init(kboot, (psf2_header_t *) &_binary_font_psf_start);

  /* set up tty display */
  tty_terminal_init();
  stdout = TTY_stdout; /* linking out stdout to tty display */

  /* set up filesystem */
  vfs_init();

  printf("sol1 kernel\n");

  printf("x87 FPU : %s\n", sys_x87_supported ? "supported" : "not supported");
  if(!sys_x87_supported)
  {
    /* Kernel panic, no FPU */
    PANIC("x87 FPU not supported");
  }

  printf("physical kernel size : %X\n", physical_kernel_sz());
  printf("early heap address : %p\n", (VMA(physical_kernel_sz() + PMM_STACK_SIZE)));
  printf("heap address : %p (initial heap size : %u B)\n", (VMA(virtual_kernel_sz())), KERNEL_DEFAULT_HEAP_SIZE);

  printf("Memory regions : \n");
  for(i = 0; i < kboot->memory_map_count; i++)
  {
    printf("  [%u] base : %X, length : %X [%s]\n", i, kboot->memory_map[i].base, kboot->memory_map[i].length,
      (kboot->memory_map[i].type == 1) ? "AVAILABLE" : "RESERVED");
  }

  printf("\n");

  printf("screen size : %u x %u\n", mbi->framebuffer_width, mbi->framebuffer_height);
  printf("screen pitch : %u\n", mbi->framebuffer_pitch);
  printf("framebuffer size : %lu B (hex:%x)\n", mbi->framebuffer_pitch * mbi->framebuffer_height,
    mbi->framebuffer_pitch * mbi->framebuffer_height);

  fflush(stdout);

  /* 50 Hz is 20ms by tick */
  printf("initializing multitasking...\n");
  tasking_init(50);

  // /* testing multitasking */
  // spawn_process(NULL, (uintptr_t) random_task, NULL, PROC_READY);
  // spawn_process(NULL, (uintptr_t) random_task, NULL, PROC_READY);
  // spawn_process(NULL, (uintptr_t) random_task, NULL, PROC_READY);
  // spawn_process(NULL, (uintptr_t) random_task, NULL, PROC_READY);
  // spawn_process(NULL, (uintptr_t) random_task, NULL, PROC_READY);

  for(i = 0; i<50; i++)
  {
    for(j = 0; j<50; j++)
    {
      set_point(i+700, j+200, rgbtoi(255, 255, 0));
    }
  }

  /* everything is done, start init process */
  solk_init();
}

/* TEMP */
void solk_init()
{


  /* infinite loop */
  while(1) {}
}

void __attribute__((noreturn)) kernel_panic(uint32_t errcode, const char *errmsg, const char *file, int line)
{
  lfb_clr();
  char buf[512];
  if(file != NULL)
  {
    sprintf(buf, "KERNEL PANIC\n\nerror: %s\nerror code: %s%d\nfile: %s:%d", errmsg,
      ((errcode == 0) & (errno != 0)) ? "(errnor)" : "", ((errcode == 0) & (errno != 0)) ? errno : errcode, file, line);
  } else
  {
    sprintf(buf, "KERNEL PANIC\n\nerror: %s\nerror code: %s%d", errmsg,
      ((errcode == 0) & (errno != 0)) ? "(errnor)" : "", ((errcode == 0) & (errno != 0)) ? errno : errcode);
  }

  lfb_write(buf, 0, 0);
  _panic_halt_loop();

  /* tell the compiler that we can't reach that point */
  __builtin_unreachable();
}
