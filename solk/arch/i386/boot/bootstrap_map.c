#include <stdint.h>

#include <solk/sys/mem.h>
#include <solk/arch/i386/mem/mem.h>

#include <solk/kernel.h>

#define P (0x1)
#define RW (0x2)

extern char __kernel_end;

/* setup bootstrap paging structure */
void bootstrap_map(uint32_t *_PD, uint32_t *_PT_DATA)
{
  /* page directory entries needed to map kernel */
  uint32_t entries = (virtual_kernel_sz() + TABLE_MASK) / TABLE_SIZE;

  unsigned int i = 0;

  /* identity map kernel pages */
  for(i = 0; i<entries * 1024; i++)
  {
    _PT_DATA[i] = (i * PAGESIZE) | P | RW;
  }

  /* map lower half */
  for(i = 0; i<entries; i++)
  {
    _PD[i] = (((uint32_t) _PT_DATA) + i*PAGESIZE) | P | RW;
  }

  /* map higher half (0xC0000000 -> index 768) */
  for(i = 0; i<entries; i++)
  {
    _PD[768 + i] = (((uint32_t) _PT_DATA) + i*PAGESIZE) | P | RW;
  }

  return;
}
