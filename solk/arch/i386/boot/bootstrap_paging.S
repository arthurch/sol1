.section .data
/* bootstrap page directory structure */
/* CARE USING _BS_PD SYMBOL AS IT REPRESENT A PHYSICAL ADDRESS */
.global _BS_PD
.align 4096
_BS_PD:
  .skip 4096

.align 4096
_BS_PT_DATA:
  .skip 102400 /* some space to store kernel page tables (25 tables = 102400 B) */

.section .text

/* _init_paging function */
.global _init_paging
.type _init_paging, @function
_init_paging:
  /* zeroing out _BS_PD */
  mov $0, %eax
1:
  movl $0, _BS_PD(%eax)
  add $4, %eax
  cmp $4096, %eax
  jne 1b

  /* zeroing out _BS_PT_DATA */
  mov $0, %eax
2:
  movl $0, _BS_PT_DATA(%eax)
  add $4, %eax
  cmp $102400, %eax
  jne 2b

  /* set up bootstrap paging structure */
  push $_BS_PT_DATA
  push $_BS_PD
  call bootstrap_map # bootstrap_map(uint32_t *_PD, uint32_t *_PT_DATA)
  pop %eax
  pop %eax

  /* use bootstrap page directory */
  movl $_BS_PD, %eax
  movl %eax, %cr3

  /* enabling paging (bit 31 in cr0 register), we also set WP bit (bit 16) */
  movl %cr0, %eax
  orl $0x80010000, %eax
  movl %eax, %cr0

  ret
