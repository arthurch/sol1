#include <solk/arch/i386/cpu/cpu.h>
#include <solk/arch/i386/cpu/pic.h>

#include <solk/sys/cpu.h>
#include <solk/sys/process.h>

#include <stdio.h>

#define PIT_CHANNEL0_PORT 0x40
#define PIT_CHANNEL1_PORT 0x41 /* unused */
#define PIT_CHANNEL2_PORT 0x42 /* unused */
#define PIT_COMMAND_PORT 0x43

#define PIT_FREQUENCY 1193182 /* PIT frequency in Hz (approximated value) */
#define PIT_REAL_FREQUENCY (3579545 / 3) /* real value */

#define PIT_MIN_FREQ 18
#define PIT_MAX_FREQ PIT_FREQUENCY

static uint32_t irq0_reload_value; /* reload value for channel 0 */
/* time between two IRQs as nanoseconds */
static uint32_t irq0_ns;
/* real frequency (for displaying purpose) */
static uint32_t irq0_real_freq;

uint32_t pit_get_reload_value(uint32_t freq)
{
  if(freq < PIT_MIN_FREQ)
    return 65536; /* (max value) */
  if(freq > PIT_MAX_FREQ)
    return 1;

  /* reload value is the number of ticks of PIT_FREQUENCY needed to
    get the requested frequency (basically PIT_FREQUENCY / freq) */
  uint16_t reload_value = PIT_REAL_FREQUENCY / freq;
  return reload_value;
}

static uint64_t nanos;

uint64_t arch_cpu_nanos()
{
  return nanos;
}

uint64_t get_pit_nanos()
{
  return nanos;
}

void pit_sleep(uint32_t sl_ms)
{
  uint64_t timer = get_pit_nanos();
  while(get_pit_nanos() - timer < (sl_ms*1000))
  {
    io_wait();
  }
}

/*
 * PIT handler is the process scheduler
 */
void irq0_pit_handler()
{
  /* update time data */
  if(nanos > (__UINT64_MAX__ - 2*irq0_ns))
    nanos = 0;
  nanos += irq0_ns;

  /* awake sleeping processes */
  awake_sleepers(nanos);

  /* end of interrupt routine */
  pic_send_eoi(0);

  /* schedule */
  switch_next();
}

uint32_t pit_get_real_freq()
{
  return irq0_real_freq;
}

void setup_pit(uint32_t frequency)
{
  nanos = 0;

  /* calculate number of ticks corresponding to requested frequency */
  uint32_t reload_value = pit_get_reload_value(frequency);
  irq0_reload_value = (uint16_t) reload_value;
  /* calculate real frequency */
  irq0_real_freq = PIT_FREQUENCY / reload_value;

  /* calculate time between two IRQs */
  /*
    formula : time in ns = reload_value / (3579545/3) * 1000000

    time in ns = reload_value * 3000000 / 3579545
    3000000 / 3579545 = 0.8380953445 */

  irq0_ns = (uint64_t) (reload_value * (0.8380953445));

  /* initialize pit :
    use channel 0, lowbyte / highbyte, rate generator
    0b00110100
    set channel 0 counter to reload_value */

  arch_disable_interrupts();

  if(reload_value >= 65536)
    reload_value = 0;
  outb(PIT_COMMAND_PORT, 0b00110100); /* channel 0, lobyte/hibyte, rate generator */
  io_wait();
  outb(PIT_CHANNEL0_PORT, (uint8_t) (reload_value & 0xFF));
  io_wait();
  outb(PIT_CHANNEL0_PORT, (uint8_t) ((reload_value >> 8) & 0xFF));
  io_wait();

  arch_enable_interrupts();

  pic_unmask_irq(0);
}
