#include <solk/arch/i386/cpu/cpu.h>
#include <solk/arch/i386/cpu/pic.h>

#include <solk/sys/cpu.h>

#include <stdio.h>

void setup_int_gate(size_t index, uintptr_t entry)
{
  if(index > IDT_MAX_ENTRIES)
    return;
  IDT[index].selector = RING0_CODE_SEGMENT_SELECTOR;
  IDT[index].type_attr = IDT_PRESENT | (CPU_RING_0 << 5) | IDT_INTERRUPT_GATE;
  IDT[index].offset_1 = entry & 0xFFFF;
  IDT[index].offset_2 = (entry >> 16) & 0xFFFF;
  IDT[index].zero = 0;
}

extern void _panic_halt_loop();
void isr_exception_handler(uint32_t vector, uint32_t errcode, uintptr_t ret_addr)
{
  catch_exception(vector, errcode, ret_addr);
}

/* Interrupt request handler */
void irq_handler(uint32_t vector)
{
  printf("got some irqs here (%u)\n", vector);
  fflush(stdout);
  pic_send_eoi(vector);
}

void isr_software_handler(uint32_t syscall_nb, uint32_t arg1, uint32_t arg2, uint32_t arg3)
{
  printf("system call %u (%u, %u, %u)\n", syscall_nb, arg1, arg2, arg3);
  fflush(stdout);
}

/* all exception handlers */
extern void _isr_exception_0(void);
extern void _isr_exception_1(void);
extern void _isr_exception_2(void);
extern void _isr_exception_3(void);
extern void _isr_exception_4(void);
extern void _isr_exception_5(void);
extern void _isr_exception_6(void);
extern void _isr_exception_7(void);
extern void _isr_exception_8(void);
extern void _isr_exception_9(void);
extern void _isr_exception_10(void);
extern void _isr_exception_11(void);
extern void _isr_exception_12(void);
extern void _isr_exception_13(void);
extern void _isr_exception_14(void);
extern void _isr_exception_15(void);
extern void _isr_exception_16(void);
extern void _isr_exception_17(void);
extern void _isr_exception_18(void);
extern void _isr_exception_19(void);
extern void _isr_exception_20(void);
extern void _isr_exception_21(void);
extern void _isr_exception_22(void);
extern void _isr_exception_23(void);
extern void _isr_exception_24(void);
extern void _isr_exception_25(void);
extern void _isr_exception_26(void);
extern void _isr_exception_27(void);
extern void _isr_exception_28(void);
extern void _isr_exception_29(void);
extern void _isr_exception_30(void);
extern void _isr_exception_31(void);

/* softare interrupt handlers */
extern void _isr_software_128(void);

/* set up interrupt service routines */
void setup_isrs()
{
  /* exceptions */
  setup_int_gate(0x0, (uintptr_t) _isr_exception_0);
  setup_int_gate(0x1, (uintptr_t) _isr_exception_1);
  setup_int_gate(0x2, (uintptr_t) _isr_exception_2);
  setup_int_gate(0x3, (uintptr_t) _isr_exception_3);
  setup_int_gate(0x4, (uintptr_t) _isr_exception_4);
  setup_int_gate(0x5, (uintptr_t) _isr_exception_5);
  setup_int_gate(0x6, (uintptr_t) _isr_exception_6);
  setup_int_gate(0x7, (uintptr_t) _isr_exception_7);
  setup_int_gate(0x8, (uintptr_t) _isr_exception_8);
  setup_int_gate(0x9, (uintptr_t) _isr_exception_9);
  setup_int_gate(0xA, (uintptr_t) _isr_exception_10);
  setup_int_gate(0xB, (uintptr_t) _isr_exception_11);
  setup_int_gate(0xC, (uintptr_t) _isr_exception_12);
  setup_int_gate(0xD, (uintptr_t) _isr_exception_13);
  setup_int_gate(0xE, (uintptr_t) _isr_exception_14);
  setup_int_gate(0xF, (uintptr_t) _isr_exception_15);
  setup_int_gate(0x10, (uintptr_t) _isr_exception_16);
  setup_int_gate(0x11, (uintptr_t) _isr_exception_17);
  setup_int_gate(0x12, (uintptr_t) _isr_exception_18);
  setup_int_gate(0x13, (uintptr_t) _isr_exception_19);
  setup_int_gate(0x14, (uintptr_t) _isr_exception_20);
  setup_int_gate(0x15, (uintptr_t) _isr_exception_21);
  setup_int_gate(0x16, (uintptr_t) _isr_exception_22);
  setup_int_gate(0x17, (uintptr_t) _isr_exception_23);
  setup_int_gate(0x18, (uintptr_t) _isr_exception_24);
  setup_int_gate(0x19, (uintptr_t) _isr_exception_25);
  setup_int_gate(0x1A, (uintptr_t) _isr_exception_26);
  setup_int_gate(0x1B, (uintptr_t) _isr_exception_27);
  setup_int_gate(0x1C, (uintptr_t) _isr_exception_28);
  setup_int_gate(0x1D, (uintptr_t) _isr_exception_29);
  setup_int_gate(0x1E, (uintptr_t) _isr_exception_30);
  setup_int_gate(0x1F, (uintptr_t) _isr_exception_31);

  /* software interrupts */
  setup_int_gate(SOFTWARE_INT_VECTOR, (uintptr_t) _isr_software_128);
}
