#include <solk/arch/i386/cpu/cpu.h>
#include <solk/arch/i386/cpu/pic.h>

extern void _irq_0(void);
extern void _irq_1(void);
extern void _irq_2(void);
extern void _irq_3(void);
extern void _irq_4(void);
extern void _irq_5(void);
extern void _irq_6(void);
extern void _irq_7(void);
extern void _irq_8(void);
extern void _irq_9(void);
extern void _irq_10(void);
extern void _irq_11(void);
extern void _irq_12(void);
extern void _irq_13(void);
extern void _irq_14(void);
extern void _irq_15(void);

void setup_pic()
{
  /* save masks */
  unsigned char a1, a2;
  a1 = inb(PIC1_DATA);
  a2 = inb(PIC2_DATA);

  /* ICW1 : init code */
  outb(PIC1_COMMAND, ICW1_INIT | ICW1_ICW4);
  io_wait();
  outb(PIC2_COMMAND, ICW1_INIT | ICW1_ICW4);
  io_wait();

  /* ICW2 : vector offsets */
  outb(PIC1_DATA, IRQ_MASTER_OFFSET);
  io_wait();
  outb(PIC2_DATA, IRQ_SLAVE_OFFSET);
  io_wait();

  /* ICW3 */
  outb(PIC1_DATA, 4); /* tell master PIC that there is a slave PIC at IRQ2 (0000 0100) */
  io_wait();
  outb(PIC2_DATA, 2); /* tell slave PIC its cascade identity (0000 0010) */
  io_wait();

  /* ICW4 : use 8086 mode */
  outb(PIC1_DATA, ICW4_8086);
  io_wait();
  outb(PIC2_DATA, ICW4_8086);
  io_wait();

  /* restore saved masks */
  outb(PIC1_DATA, a1);
  io_wait();
  outb(PIC2_DATA, a2);
  io_wait();

  /* add interrupt gates to IDT */
  setup_int_gate(IRQ_MASTER_OFFSET, (uintptr_t) _irq_0);
  setup_int_gate(IRQ_MASTER_OFFSET + 1, (uintptr_t) _irq_1);
  setup_int_gate(IRQ_MASTER_OFFSET + 2, (uintptr_t) _irq_2);
  setup_int_gate(IRQ_MASTER_OFFSET + 3, (uintptr_t) _irq_3);
  setup_int_gate(IRQ_MASTER_OFFSET + 4, (uintptr_t) _irq_4);
  setup_int_gate(IRQ_MASTER_OFFSET + 5, (uintptr_t) _irq_5);
  setup_int_gate(IRQ_MASTER_OFFSET + 6, (uintptr_t) _irq_6);
  setup_int_gate(IRQ_MASTER_OFFSET + 7, (uintptr_t) _irq_7);
  setup_int_gate(IRQ_SLAVE_OFFSET, (uintptr_t) _irq_8);
  setup_int_gate(IRQ_SLAVE_OFFSET + 1, (uintptr_t) _irq_9);
  setup_int_gate(IRQ_SLAVE_OFFSET + 2, (uintptr_t) _irq_10);
  setup_int_gate(IRQ_SLAVE_OFFSET + 3, (uintptr_t) _irq_11);
  setup_int_gate(IRQ_SLAVE_OFFSET + 4, (uintptr_t) _irq_12);
  setup_int_gate(IRQ_SLAVE_OFFSET + 5, (uintptr_t) _irq_13);
  setup_int_gate(IRQ_SLAVE_OFFSET + 6, (uintptr_t) _irq_14);
  setup_int_gate(IRQ_SLAVE_OFFSET + 1, (uintptr_t) _irq_15);

  /* mask all irqs for now */
  for(size_t i = 0; i<16; i++)
    pic_mask_irq(i);
}

void pic_mask_irq(uint16_t irq)
{
  uint16_t port;
  uint16_t masks;

  if(irq < 8)
  {
    port = PIC1_DATA;
  } else if(irq < 16)
  {
    port = PIC2_DATA;
    irq -= 8;
  } else
  {
    return;
  }

  masks = inb(port) | (1 << irq);
  outb(port, masks);
  io_wait();
}

void pic_unmask_irq(uint16_t irq)
{
  uint16_t port;
  uint16_t masks;

  if(irq < 8)
  {
    port = PIC1_DATA;
  } else if(irq < 16)
  {
    port = PIC2_DATA;
    irq -= 8;
  } else
  {
    return;
  }

  masks = inb(port) & ~(1 << irq);
  outb(port, masks);
  io_wait();
}
