#include <solk/sys/cpu.h>
#include <solk/arch/i386/cpu/cpu.h>
#include <solk/arch/i386/cpu/pic.h>

#include <types.h>
#include <lib.h>

struct idt_entry IDT[IDT_MAX_ENTRIES];

struct idt_pointer {
  uint16_t limit;
  uint32_t base;
} __attribute__((packed)) idt_ptr;

extern void _load_idt(uintptr_t);

void arch_enable_interrupts()
{
  asm volatile ("sti");
}

void arch_disable_interrupts()
{
  asm volatile ("cli");
}

/* set up interrupt descriptor table */
void setup_idt()
{
  memset((void *) IDT, 0, sizeof(struct idt_entry) * IDT_MAX_ENTRIES);
  idt_ptr.limit = sizeof(struct idt_entry) * IDT_MAX_ENTRIES - 1;
  idt_ptr.base = (uint32_t) IDT;
  _load_idt((uintptr_t) &idt_ptr);
}

void arch_cpu_setup()
{
  setup_idt();
  setup_pic();
  setup_isrs();

  arch_enable_interrupts();
}
