#include <solk/arch/i386/cpu/cpu.h>

#include <solk/panic.h>
#include <solk/mm/vmm.h>

#include <types.h>

static const char *cpu_exceptions[32] = {
  /* 0x00 */ "#DE: Divide Error",
  /* 0x01 */ "#DB: Debug Exception",
  /* 0x02 */ "NMI Interrupt",
  /* 0x03 */ "#BP: Breakpoint",
  /* 0x04 */ "#OF: Overflow",
  /* 0x05 */ "#BR: BOUND Range Exceeded",
  /* 0x06 */ "#UD: Invalid Opcode (Undefined Opcode)",
  /* 0x07 */ "#NM: Device Not Available (No Math Coprocessor)",
  /* 0x08 */ "#DF: Double Fault",
  /* 0x09 */ "Coprocessor Segment Overrun (reserved)",
  /* 0x0a */ "#TS: Invalid TSS",
  /* 0x0b */ "#NP: Segment Not Present",
  /* 0x0C */ "#SS: Stack-Segment Fault",
  /* 0x0D */ "#GP: General Protection",
  /* 0x0E */ "#PF: Page Fault",
  /* 0x0F */ "Reserved",
  /* 0x10 */ "#MF: x87 FPU Floating-Point Error (Math Fault)",
  /* 0x11 */ "#AC: Alignment Check",
  /* 0x12 */ "#MC: Machine Check",
  /* 0x13 */ "#XM: SIMD Floating-Point Exception",
  /* 0x14 */ "#VE: Virtualization Exception",
  /* 0x15 */ "Reserved",
  /* 0x16 */ "Reserved",
  /* 0x17 */ "Reserved",
  /* 0x18 */ "Reserved",
  /* 0x19 */ "Reserved",
  /* 0x1A */ "Reserved",
  /* 0x1B */ "Reserved",
  /* 0x1C */ "Reserved",
  /* 0x1D */ "Reserved",
  /* 0x1E */ "Reserved",
  /* 0x1F */ "Reserved"
};

#include <stdio.h>

extern void _panic_halt_loop();
/* error contains informations about the exceptions or 0
  if no informations are available */
void catch_exception(uint32_t code, uint32_t error, uintptr_t ret_addr)
{
  switch(code)
  {
  case 14: /* page fault (cr2 contains the address which caused the fault) */
    vmm_page_fault(error, read_cr2(), ret_addr);
    break;
  default:
    kernel_panic(error, cpu_exceptions[code], "cpu exception", code);
    break;
  }
}
