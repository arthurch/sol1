#include <solk/arch/i386/sys/task.h>

#include <solk/arch/i386/cpu/cpu.h>

#include <solk/sys/process.h>

#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <list.h>

#define PUSH(stack, type, val) stack -= sizeof(type); \
  *((type *) stack) = val;

extern uintptr_t read_eip();

void switch_task(task_t *current_task, task_t *next_task)
{
  /* save current task state */
  uintptr_t eip, ebp, esp;

  asm volatile("mov %%esp, %0":"=r"(esp)); /* read esp */
  asm volatile("mov %%ebp, %0":"=r"(ebp)); /* read ebp */

  current_task->ebp = ebp;
  current_task->esp = esp;

  eip = read_eip(); /* read eip */
  if(eip == (uintptr_t) -1) /* returned from long jump */
  {
    return;
  }
  current_task->eip = eip;

  /* switch to next task */
  eip = next_task->eip;
  esp = next_task->esp;
  ebp = next_task->ebp;

  /* jump */
  // asm volatile(
  //   "mov %0, %%ebx\n"
  //   "mov %1, %%esp\n"
  //   "mov %2, %%ebp\n"
  //   "mov $-1, %%eax\n" /* read_eip() will return -1 */
  //   "jmp *%%ebx"
  //   : : "r" (eip), "r" (esp), "r" (ebp)
  //   : "%ebx", "%eax", "%ebp");
  asm volatile(
    "mov %0, %%eax\n"
    "mov %1, %%ebx\n"
    "mov %2, %%ecx\n"
    "jmp _taskjmp" /* this subroutine will jump for us
                        (and set up stack pointers) */
    : : "r" (esp), "r" (eip), "r" (ebp)
    : "%eax", "%ebx", "%ecx");
}

extern void _ktask_start();
void spawn_task(task_t *task, uintptr_t func, uintptr_t stack)
{
  /* setup standard environment for kernel task */
  struct regs r;
  memset(&r, 0, sizeof(struct regs));
  r.esp = stack;
  r.ebp = stack;
  r.eip = func;
  r.eflags = 0x202; /* IF set */
  /* this code is supposed to be running under
    kernel mode, so cs and ss are the same as now */
  r.cs = read_cs();
  r.ss = read_ss();

  /* return to kernel task */
  task->eip = (uintptr_t) _ktask_start;
  task->ebp = stack;

  /* push environment */
  PUSH(stack, struct regs, r);
  task->esp = stack;
}

void scheduler_init(uint32_t freq)
{
  setup_pit(freq);
}
