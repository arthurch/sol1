#include <solk/arch/i386/mem/mem.h>

#include <solk/mm/vmm.h>
#include <solk/mm/kmalloc.h>
#include <solk/sys/mem.h>

#include <types.h>
#include <lib.h>

#include <stdio.h>

static uintptr_t __phys_kernel_pd;
static uintptr_t *__kernel_pagedirectory;

void arch_vmm_init()
{
  __phys_kernel_pd = (uintptr_t) read_cr3();
  __kernel_pagedirectory = (uintptr_t *) VMA(__phys_kernel_pd);
}

arch_vmspace arch_clone_kernel_vmspace()
{
  /* TODO : use real kmalloc function */
  /* !! TODO : ALLOCATE PAGE ALIGNED DATA */
  i386_vmspace_t *vm = (i386_vmspace_t *) early_kmalloc(sizeof(struct i386_vmspace));
  memset((void *) vm->pagedirectory, 0, KERNEL_VIRTUAL_INDEX);

  /* this set up recursive paging as it's the last pd entry */
  vm->phys_addr = arch_get_physaddr((uintptr_t) vm);

  for(size_t i = 0; i<(PD_ENTRIES - KERNEL_VIRTUAL_INDEX - 1); i++)
  {
    vm->pagedirectory[i + KERNEL_VIRTUAL_INDEX] = __kernel_pagedirectory[i + KERNEL_VIRTUAL_INDEX];
  }

  return (arch_vmspace) vm;
}

int arch_switch_vmspace(arch_vmspace vmspace)
{
  i386_vmspace_t *vm = (i386_vmspace_t *) vmspace;
  /* we need page aligned address */
  uintptr_t new_pd = vm->phys_addr & ~PAGE_MASK;

  if(read_cr3() != new_pd)
  {
    printf("switching to : %X\n", new_pd);
    write_cr3(new_pd);
  }

  return 0;
}
