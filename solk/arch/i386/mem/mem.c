#include <solk/arch/i386/mem/mem.h>
#include <solk/sys/mem.h>

#include <solk/mm/pmm.h>

#include <lib.h>
#include <stdio.h>

/* page utility functions */
uint32_t alloc_table()
{
  uint32_t p = pmm_allocate_page();
  return p;
}

/* get physical address using recursive paging */
uintptr_t arch_get_physaddr(uintptr_t vaddr)
{
  uint32_t pdindex = vaddr >> 22;
  uint32_t ptindex = (vaddr >> 12) & 0x3FF;

  uint32_t *pd = get_pd_recursive();
  if(PAGE_ENTRY_PRESENT(pd[pdindex]) != 1)
  {
    /* no physical address mapped to this page */
    return (uintptr_t) 0;
  }

  uint32_t *pt = (uint32_t *) (0xFFC00000 + pdindex * PT_SIZE);
  if(PAGE_ENTRY_PRESENT(pt[ptindex]) != 1)
  {
    /* no physical address mapped to this page */
    return (uintptr_t) 0;
  }
  return (uintptr_t) ((pt[ptindex] & 0xFFFFF000) + (vaddr & 0xFFF));
}

uintptr_t arch_map_page(uintptr_t paddr, uintptr_t vaddr, uint32_t flags)
{
  /* get page aligned addresses */
  paddr = paddr & 0xFFFFF000;
  vaddr = vaddr & 0xFFFFF000;

  uint32_t pdindex = vaddr >> 22;
  uint32_t ptindex = (vaddr >> 12) & 0x3FF;

  uint32_t *pd = get_pd_recursive();
  if(PAGE_ENTRY_PRESENT(pd[pdindex]) != 1)
  {
    /* we need a new page table */
    uint32_t page = alloc_table();
    pd[pdindex] = (uint32_t) (page | PAGING_P | PAGING_RW);
    tlb_flush(); /* flush tlb to reset mmu's cache */

    /* we should clean it */
    uint32_t *table = (uint32_t *) (0xFFC00000 + pdindex * PT_SIZE);
    for(uint32_t i = 0; i<PT_ENTRIES; i++)
    {
      table[i] = 0;
    }
  }

  uint32_t *pt = (uint32_t *) (0xFFC00000 + pdindex * PT_SIZE);
  pt[ptindex] = (uint32_t) (paddr | (flags & 0xFFF) | PAGING_P);

  flush_tlb_single(vaddr);

  return vaddr;
}

uintptr_t arch_safe_map_page(uintptr_t paddr, uintptr_t vaddr, uint32_t flags)
{
  /* get page aligned addresses */
  paddr = paddr & 0xFFFFF000;
  vaddr = vaddr & 0xFFFFF000;

  uint32_t pdindex = vaddr >> 22;
  uint32_t ptindex = (vaddr >> 12) & 0x3FF;

  uint32_t *pd = get_pd_recursive();
  if(PAGE_ENTRY_PRESENT(pd[pdindex]) != 1)
  {
    /* this function can't allocate a new table... */
    return 0;
  }

  uint32_t *pt = (uint32_t *) (0xFFC00000 + pdindex * PT_SIZE);
  pt[ptindex] = (uint32_t) (paddr | (flags & 0xFFF) | PAGING_P);

  flush_tlb_single(vaddr);

  return vaddr;
}

void arch_unmap_page(uintptr_t vaddr)
{
  /* get page aligned address */
  vaddr = vaddr & 0xFFFFF000;

  uint32_t pdindex = vaddr >> 22;
  uint32_t ptindex = (vaddr >> 12) & 0x3FF;

  uint32_t *pd = get_pd_recursive();
  if(PAGE_ENTRY_PRESENT(pd[pdindex]) != 1)
  {
    /* page directory entry not present so no need to unmap */
    return;
  }

  uint32_t *pt = (uint32_t *) (0xFFC00000 + pdindex * PT_SIZE);
  pt[ptindex] = 0x0;

  validate_pde(pdindex);

  flush_tlb_single(vaddr);
}

void validate_pde(uint32_t pdindex)
{
  uint32_t *pd = (uint32_t *) (0xFFFFF000);
  if(PAGE_ENTRY_PRESENT(pd[pdindex]) != 1)
  {
    /* page directory entry not present */
    return;
  }

  uint32_t *pt = (uint32_t *) (0xFFC00000 + pdindex * PT_SIZE);;
  unsigned int i = 0;
  for(i = 0; i<PT_ENTRIES; i++)
  {
    if(PAGE_ENTRY_PRESENT(pt[i]) == 1)
    {
      /* page directory entry is still valid */
      return;
    }
  }
  /* if we reach this point, no page table entry is present
    we need to free that table */
  uint32_t table = pd[pdindex] & ~0xFFF;
  pmm_free_page(table);
  pd[pdindex] = 0x0;
}

/* setup kernel virtual space */
void setup_kernel_paging()
{
  /* get current page directory (created by bootstrap code) */
  uint32_t __curr_pd = read_cr3();
  uint32_t *bootstrap_pd = (uint32_t *) VMA(__curr_pd);

  /* set up recursive paging */
  bootstrap_pd[1023] = __curr_pd | PAGING_P | PAGING_RW;

  /* unmap lower half memory */
  uint32_t entries = (virtual_kernel_sz() + TABLE_MASK) / TABLE_SIZE;
  unsigned int i = 0;
  for(i = 0; i<entries; i++)
  {
    /* present = 0 */
    bootstrap_pd[i] = 0x0;
  }

  /* mapping higher half is already done by bootstrap code */

  tlb_flush();
}
