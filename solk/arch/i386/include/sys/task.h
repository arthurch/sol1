#ifndef TASK_H
#define TASK_H

#include <types.h>

#define SCHEDULER_FREQ

/*
 * Structure representing x86 task
 */
struct task {
  uintptr_t eip;
  uintptr_t ebp;
  uintptr_t esp;
};
typedef struct task task_t;

void switch_task(task_t *current_task, task_t *next_task);
void spawn_task(task_t *task, uintptr_t func, uintptr_t stack);

void scheduler_init(uint32_t scheduler_freq);

#endif
