#ifndef i386_MEM_H
#define i386_MEM_H

#include <stdint.h>
#include <solk/arch/i386/cpu/cpu.h>
#include <solk/sys/mem.h>

#define PAGING_P (1)
#define PAGING_RW (1 << 1)

#define PT_SIZE (4096)
#define PD_ENTRIES (1024)
#define PT_ENTRIES (1024)

/* 1024 * 4096 B tables */
#define TABLE_SIZE (1024 * PAGESIZE)
#define TABLE_MASK (TABLE_SIZE - 1)

#define PAGE_ENTRY_PRESENT(E) (E & PAGING_P)

#define KERNEL_VIRTUAL_INDEX (KERNEL_VMA >> 22)

/* represent a page directory can be seen as a array of 1024 PHYSICAL addresses */
struct i386_vmspace {
  uintptr_t pagedirectory[PD_ENTRIES-1]; /* 1023 addresses */
  uintptr_t phys_addr; /* last address points to the page directory itself */
};
typedef struct i386_vmspace i386_vmspace_t;

volatile static inline void tlb_flush(void)
{
  write_cr3(read_cr3());
}

volatile static inline void flush_tlb_single(uintptr_t addr)
{
  asm volatile("invlpg (%0)" ::"r" (addr) : "memory");
}

static inline uint32_t *get_pd_recursive()
{
  return (uint32_t *) (0xFFFFF000);
}

void validate_pde(uint32_t pdindex);

#endif
