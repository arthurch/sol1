#ifndef PIC_H
#define PIC_H

#define PIC1 0x20 /* IO base address for master PIC */
#define PIC2 0xA0 /* IO base address for slave PIC */
#define PIC1_COMMAND PIC1
#define PIC1_DATA (PIC1+1)
#define PIC2_COMMAND PIC2
#define PIC2_DATA (PIC2+1)

#define PIC_COMMAND_EOI 0x20 /* End-of-interrupt command code */

/* Initialization Command Words */

/* to initialize the PIC we have 4 steps :
  - send init code (ICW1)
  - send vector offset to both master and slave PIC (ICW2)
  - tell how master PIC and slave PIC are wired (ICW3)
  - give additionnal informations about environment (ICW4) */

/* default values are in parenthesis */
#define ICW1_ICW4  0x01        /* ICW4 (not) needed */
#define ICW1_SINGLE  0x02      /* Single (cascade) mode */
#define ICW1_INTERVAL4 0x04    /* Call address interval 4 (8) */
#define ICW1_LEVEL 0x08       /* Level triggered (edge) mode */
#define ICW1_INIT 0x10        /* Initialization - required! */

#define ICW4_8086  0x01        /* 8086/88 (MCS-80/85) mode */
#define ICW4_AUTO  0x02        /* Auto (normal) EOI */
#define ICW4_BUF_SLAVE 0x08    /* Buffered mode/slave */
#define ICW4_BUF_MASTER 0x0C  /* Buffered mode/master */
#define ICW4_SFNM  0x10        /* Special fully nested (not) */

static inline void pic_send_eoi(unsigned char irq)
{
  if(irq >= 8)
    outb(PIC2_COMMAND, PIC_COMMAND_EOI);
  outb(PIC1_COMMAND, PIC_COMMAND_EOI);
  io_wait();
}

void setup_pic();
void pic_mask_irq(uint16_t irq);
void pic_unmask_irq(uint16_t irq);

#endif
