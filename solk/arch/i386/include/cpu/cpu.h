#ifndef i386_CPU_H
#define i386_CPU_H

#include <stdio.h>

#include <types.h>

#define CLI asm volatile ("cli")
#define STI asm volatile ("sti")

#define CPU_RING_0 (0x0) /* kernel mode ring */
#define CPU_RING_3 (0x3) /* user mode ring */

#define RING0_CODE_SEGMENT_SELECTOR (0x08)
#define RING3_CODE_SEGMENT_SELECTOR (0x18)

void catch_exception(uint32_t code, uint32_t error, uintptr_t ret_addr);

/* registers */

/* read x86 registers */
uintptr_t read_esp();
uintptr_t read_ebp();
uintptr_t read_cs();
uintptr_t read_ss();

struct regs {
  uint32_t edi, esi, ebp, esp, ebx, edx, ecx, eax; /* standard registers */
  uint32_t int_no, err_code; /* interrupt specific informations */
  uint32_t eip, cs, eflags, useresp, ss; /* iret stack environment */
};

/* pit time system (could be unsafe, at least not accurate) */
uint64_t get_pit_nanos();
/* this one is even worse */
void pit_sleep(uint32_t sl_ms);

/* PIT */
void setup_pit(uint32_t);

/* IDT / ISRs / IRQs */

#define IDT_INTERRUPT_GATE (0b1110)
#define IDT_TRAP_GATE (0b1111)
#define IDT_PRESENT (1<<7)
#define IDT_MAX_ENTRIES (256)

#define IRQ_MASTER_OFFSET 0x20
#define IRQ_SLAVE_OFFSET 0x28

struct idt_entry {
  uint16_t offset_1;  /* offset bits 0..15 */
  uint16_t selector;  /* code segment selector */
  uint8_t zero;       /* unused */
  uint8_t type_attr;  /* type and attributes */
  uint16_t offset_2;  /* offset bits 16..31 */
} __attribute__((packed));

extern struct idt_entry IDT[IDT_MAX_ENTRIES];

void setup_isrs();
void setup_int_gate(size_t index, uintptr_t entry);

/* I/O Ports */

static inline void outb(uint16_t port, uint8_t val)
{
    asm volatile ( "outb %0, %1" : : "a"(val), "Nd"(port) );
}

static inline void outw(uint16_t port, uint16_t val)
{
    asm volatile ( "outw %0, %1" : : "a"(val), "Nd"(port) );
}

static inline void outl(uint16_t port, uint32_t val)
{
    asm volatile ( "outl %0, %1" : : "a"(val), "Nd"(port) );
}

static inline uint8_t inb(uint16_t port)
{
    uint8_t ret;
    asm volatile ( "inb %1, %0"
                   : "=a"(ret)
                   : "Nd"(port) );
    return ret;
}

static inline uint8_t inw(uint16_t port)
{
    uint16_t ret;
    asm volatile ( "inw %1, %0"
                   : "=a"(ret)
                   : "Nd"(port) );
    return ret;
}

static inline uint8_t inl(uint16_t port)
{
    uint32_t ret;
    asm volatile ( "inl %1, %0"
                   : "=a"(ret)
                   : "Nd"(port) );
    return ret;
}

static inline void io_wait(void)
{
    /* This may be fragile. */
    asm volatile ( "jmp 1f\n\t"
                   "1:jmp 2f\n\t"
                   "2:" );
}

/* Reg dump */

static inline void i386_dump_registers(struct regs *regs)
{
  printf("Registers dump:\n");
  printf("edi = %p\n", regs->edi);
  printf("esi = %p\n", regs->esi);
  printf("ebp = %p\n", regs->ebp);
  printf("esp = %p\n", regs->esp);
  printf("ebx = %p\n", regs->ebx);
  printf("edx = %p\n", regs->ecx);
  printf("ecx = %p\n", regs->edx);
  printf("eax = %p\n", regs->eax);
  printf("eip = %p\n", regs->eip);
  printf("cs  = %p\n", regs->cs );
  printf("eflags = %p\n", regs->eflags);
  printf("useresp = %p\n", regs->useresp);
  printf("ss  = %p\n", regs->ss);
}

/* CR0 */
#define CR0_PG  (1 << 31)
#define CR0_MP  (1 << 1)
#define CR0_EM  (1 << 2)
#define CR0_NE  (1 << 5)

/* CR4 */
#define CR4_PSE (1 << 4)

/* access control registers */
static inline uintptr_t read_cr0(void)
{
  uint32_t retval = 0;
  asm volatile("mov %%cr0, %%eax":"=a"(retval));
  return retval;
}

static inline uintptr_t read_cr1(void)
{
  uintptr_t retval = 0;
  asm volatile("mov %%cr1, %%eax":"=a"(retval));
  return retval;
}

static inline uintptr_t read_cr2(void)
{
  uintptr_t retval = 0;
  asm volatile("mov %%cr2, %%eax":"=a"(retval));
  return retval;
}

volatile static inline uintptr_t read_cr3(void)
{
  uintptr_t retval = 0;
  asm volatile("mov %%cr3, %%eax":"=a"(retval));
  return retval;
}

static inline uintptr_t read_cr4(void)
{
  uintptr_t retval = 0;
  asm volatile("mov %%cr4, %%eax":"=a"(retval));
  return retval;
}

static inline void write_cr0(uintptr_t val)
{
  asm volatile("mov %%eax, %%cr0"::"a"(val));
}

static inline void write_cr1(uintptr_t val)
{
  asm volatile("mov %%eax, %%cr1"::"a"(val));
}

static inline void write_cr2(uintptr_t val)
{
  asm volatile("mov %%eax, %%cr2"::"a"(val));
}

volatile static inline void write_cr3(uintptr_t val)
{
  asm volatile("mov %%eax, %%cr3"::"a"(val));
}

static inline void write_cr4(uintptr_t val)
{
  asm volatile("mov %%eax, %%cr4"::"a"(val));
}

#endif
