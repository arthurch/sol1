KERNEL_ARCH_OBJS:=\
	$(ARCHDIR)/boot/boot.o $(ARCHDIR)/boot/bootstrap_paging.o $(ARCHDIR)/boot/bootstrap_map.o \
	$(ARCHDIR)/mem/vmm.o $(ARCHDIR)/mem/mem.o \
	$(ARCHDIR)/cpu/isr.o $(ARCHDIR)/cpu/interrupts.o $(ARCHDIR)/cpu/cpu.o $(ARCHDIR)/cpu/pic.o $(ARCHDIR)/cpu/exceptions.o \
	$(ARCHDIR)/cpu/pit.o \
	$(ARCHDIR)/sys/task.o $(ARCHDIR)/sys/sys.o \
	$(ARCHDIR)/_kernel_init.o

ARCH_OBJS:=\

ARCH_INCLUDE_DIR:=$(ARCHDIR)/include
