#include <solk/sys/process.h>

#include <solk/kernel.h>
#include <solk/panic.h>
#include <solk/sys/cpu.h>

#include <malloc.h>
#include <stdio.h>
#include <string.h>
#include <bitset.h>
#include <list.h>
#include <tree.h>

char *init_process_name = "init";
char *default_thread_name = "main";
char *default_process_name = "[unamed]";

/* ready processes list */
static list_t *ready_list;
/* paused processes list */
static list_t *waiting_list;
/* sleeping processes list (processes
  waiting to be awaken after a given
  duration) */
static list_t *sleeping_list;

static process_t *init;

static process_t *current_process;

/* bitset representing used pids */
static bitset_t *pid_bitset;
/* keep a link to every process in a tree structure */
static tree_t *process_tree;

/*
 * Initialize multitasking
 */
void tasking_init(uint32_t scheduler_freq)
{
  ready_list = list_create();
  waiting_list = list_create();
  sleeping_list = list_create();

  pid_bitset = bitset_create(SYS_MAX_PID);

  process_tree = tree_create();

  spawn_init();
  current_process = init;
  init->running = 1;

  scheduler_init(scheduler_freq);
}

/*
 * Spawn init process
 */
void spawn_init()
{
  init = (process_t *) malloc(sizeof(process_t));
  memset(init, 0, sizeof(process_t));

  init->pid = 0; /* pid 0 is reserved for init process */
  bitset_set(pid_bitset, 0);
  init->name = init_process_name;

  spawn_thread(&(init->thread), (uintptr_t) solk_init, NULL);

  make_process_ready(init);

  if(process_tree->length != 0)
  {
    tree_empty(process_tree);
  }
  tree_root(process_tree, init);
}

/**** Schedule ****/

/*
 * Get next process ready to be executed
 */
static process_t *next_ready_process()
{
  node_t *curr_node = current_process->ready_node;
  if(curr_node->owner != ready_list)
  {
    /* oops */
    return (process_t *) ready_list->head->value;
  }

  process_t *next;
  if(curr_node->fwd == NULL)
  {
    if(ready_list->length == 0)
    {
      PANIC("No processes are ready what am i supposed to do ?!");
    }

    /* rewind */
    return (process_t *) ready_list->head->value;
  }
  next = (process_t *) curr_node->fwd->value;

  if(next->state != PROC_READY)
  {
    /* why would you ? ... whatever just pause it */
    pause_process(next, 0);
    return next_ready_process();
  }

  return next;
}

/*
 * Schedule
 */
void switch_next()
{
  process_t *next = next_ready_process();
  next->running = 1;
  current_process->running = 0;

  process_t *last = current_process;

  current_process = next;

  switch_task(&(last->thread.task), &(next->thread.task));
}

/**** Manage processes ****/

/*
 * Get pid of given process, if p is NULL, return
 * current process' pid
 * @return process' pid
 */
pid_t getpid(process_t *p)
{
  if(p == NULL)
    return current_process->pid;
  return p->pid;
}

static uint8_t tree_compare_pid(void *v1, uintptr_t v2)
{
  return ((process_t *) v1)->pid == (pid_t) v2;
}
static uint8_t tree_compare_name(void *v1, uintptr_t v2)
{
  return strcmp(((process_t *) v1)->name, (char *) v2) == 0;
}

/*
 * Find a process by pid
 * @return found process or NULL
 */
process_t *getprocess(pid_t pid)
{
  tree_node_t *found = tree_find_custom(process_tree, (uintptr_t) pid, tree_compare_pid);
  if(found == NULL) return NULL;
  return (process_t *) found->value;
}

/*
 * Find a process by name
 * @return found process or NULL
 */
process_t *getprocess_by_name(const char *name)
{
  tree_node_t *found = tree_find_custom(process_tree, (uintptr_t) name, tree_compare_name);
  if(found == NULL) return NULL;
  return (process_t *) found->value;
}

process_t *getcurrentprocess()
{
  return current_process;
}

/*
 * Spawn thread
 */
void spawn_thread(thread_t *th, uintptr_t entry_point, char *name)
{
  th->tid = 0; /* each thread has id 0 for now */

  /* allocate new kernel stack */
  th->stack = (uintptr_t) kvalloc(KERNEL_STACK_SIZE, STACK_ALIGN);

  th->user_stack = 0; /* UNUSED */
  spawn_task(&(th->task), (uintptr_t) entry_point, th->stack);

  th->name = (name == NULL) ? default_thread_name : name;
}

/*
 * Get a usable pid
 * @return a usable pid
 */
static pid_t get_next_pid()
{
  pid_t pid = bitset_get_first_clear(pid_bitset);
  if(pid == SYS_MAX_PID)
  {
    PANIC("no more pids are available ?!");
  }
  bitset_set(pid_bitset, pid);
  return pid;
}

/* maybe we should implement fork and clone functions instead of this */
/*
 * Spawn process, and make it ready to be executed if ready is PROC_READY
 * if parent is NULL, parent process is set to current running process
 */
process_t *spawn_process(process_t *parent, uintptr_t entry_point, char *name, uint8_t ready)
{
  process_t *proc = (process_t *) malloc(sizeof(process_t));
  memset(proc, 0, sizeof(process_t));

  proc->pid = get_next_pid();
  proc->name = (name == NULL) ? default_process_name : name;

  if(parent == NULL)
  {
    parent = current_process;
  }
  proc->parent = parent;

  /* add process to process_tree */
  tree_node_t *pnode = tree_find(process_tree, parent);
  if(pnode == NULL)
  {
    printf("[ERROR] parent(%p) is not in process_tree, appending it to init\n", parent);
    proc->parent = init;
    tree_append(pnode, init);
  } else
  {
    tree_append(pnode, proc);
  }

  spawn_thread(&(proc->thread), entry_point, NULL);

  if(ready == PROC_READY)
  {
    make_process_ready(proc);
  } else
  {
    pause_process(proc, 0);
  }

  return proc;
}

/*
 * Pause current process for a given amount
 * of time (in milliseconds)
 */
void process_sleep(uint64_t millis)
{
  pause_process(current_process, millis);
}

/*
 * Pause a process (if sleep is non-zero, the
 * process will be awaken after the given duration
 * in milliseconds)
 */
void pause_process(process_t *proc, uint64_t sleep)
{
  if(proc == NULL || proc->state != PROC_READY)
  {
    /* process is NULL or already waiting */
    return;
  }

  if(sleep == 0)
  {
    /* pause process */
    proc->paused_node = list_append(waiting_list, proc);
    proc->wait_until = 0;
  } else
  {
    /* make process sleep for the given duration */
    proc->paused_node = list_append(sleeping_list, proc);
    proc->wait_until = arch_cpu_nanos() + sleep * 1000; /* sleep is in ms */
  }

  proc->state = PROC_PAUSED;
  if(proc->ready_node != NULL)
    list_remove_node(proc->ready_node);

  if(proc == current_process)
  {
    proc->running = 0;

    /* reschedule */
    switch_next();
  }
}

/*
 * Awake a process
 */
void resume_process(process_t *proc)
{
  if(proc == NULL || proc->state == PROC_READY)
  {
    /* process is NULL or already awaken */
    return;
  }

  if(list_find(ready_list, proc) != NULL)
  {
    /* proc is already in ready_list ?!
      you should not change process states
      manually */
    proc->state = PROC_READY;
    proc->wait_until = 0;
    return;
  }

  /* resume paused process */
  proc->state = PROC_READY;
  list_remove_node(proc->paused_node);
  proc->ready_node = list_append(ready_list, proc);
  proc->wait_until = 0;
}

/*
 * Loop throught sleeping_list and awake
 * processes if their sleeping duration is over
 */
void awake_sleepers(uint64_t current_time)
{
  process_t *p;
  foreach(sleeping_list, n)
  {
    p = (process_t *) n->value;
    if(p->state != PROC_PAUSED || (current_time > p->wait_until))
    {
      resume_process(p);
    }
  }
}

void make_process_ready(process_t *proc)
{
  proc->ready_node = list_append(ready_list, (void *) proc);
  proc->state = PROC_READY;
  proc->running = 0;
}

/*
 * Remove and process and free it
 */
void destroy_process(process_t *proc)
{
  if(__builtin_expect(proc == NULL, 0))
  {
    return;
  }

  if(proc->state == PROC_READY)
  {
    list_remove_node(proc->ready_node);
  }

  /* pid is now free for use */
  bitset_clear(pid_bitset, proc->pid);

  free(proc);
}
