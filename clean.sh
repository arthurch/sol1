#!/bin/bash

NEVER_CLEAN="slibc"

if [[ -z $CONF_DONE ]]; then
  . ./configure.sh
fi

ALL=1
CLEAN_OPTION=cleanall

if [[ $# -ge 1 ]]; then
  if [[ $1 -eq "soft" ]]; then
    ALL=0
    CLEAN_OPTION=clean
  fi
fi

for PROJECT in $PROJDIR; do
  if [[ $NEVER_CLEAN != *"${PROJECT}"* || $CLEAN_OPTION == "cleanall" ]]; then
    echo "cleaning ${PROJECT}..."
    (cd ${PROJECT} && make ${CLEAN_OPTION})
  fi
done

if [[ $ALL -eq 1 ]]; then
  echo -n "Removing compiled binaries..."
  rm -rf sysroot/
  rm -rf isodir/
  rm -f *.iso
  echo "done"
fi
