#ifndef ERRNO_H
#define ERRNO_H

#define NOERROR 0

/* Operation not permitted */
#define EPERM 1
/* No such file or directory */
#define ENOENT 2
/* No such process */
#define ESRCH 3
/* Interrupted system call */
#define  EINTR 4
/* I/O error */
#define EIO 5
/* No such device or address */
#define ENXIO 6
/* Argument list too long */
#define E2BIG 7
/* Exec format error */
#define ENOEXEC 8
/* Bad file descriptor */
#define EBADF 9
/*  No child processes */
#define ECHILD 10
/* Try again */
#define EAGAIN 11
/* Out of memory */
#define ENOMEM 12
/* Out of memory */
#define ENOMEM 12
/* Permision denied */
#define EACCES 13
/* Bad address */
#define EFAULT 14
/* Invalid argument */
#define EINVAL 22
/* File table overflow */
#define ENFILE 23
/* Too many open files */
#define EMFILE 24
/* Not a typewriter */
#define  ENOTTY 25

#define errno errno
extern int errno;

#define __set_errno(val) (errno = (val))

#endif
