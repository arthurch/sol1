#ifndef BITSET_H
#define BITSET_H

#include <types/bits.h>

#define BITSET_WORD_SIZE 32
#define BITSET_WORD_MASK (BITSET_WORD_SIZE - 1)

#define BITINDEX(index) (index / BITSET_WORD_SIZE)
#define BITOFFSET(index) (index % BITSET_WORD_SIZE)

struct bitset {
  uint32_t *data;
  size_t nwords;
  size_t length;
};
typedef struct bitset bitset_t;

bitset_t *bitset_create(size_t sz);

void bitset_set(bitset_t *bitset, size_t index);
void bitset_clear(bitset_t *bitset, size_t index);

size_t bitset_get_first_clear(bitset_t *bitset);
uint8_t bitset_test(bitset_t *bitset, size_t index);

#endif
