#ifndef MATH_H
#define MATH_H

/* calculate nearest higher power of 2 */
static inline uint32_t next_pow2(uint32_t x)
{
	if (x <= 2) return x;

	return (1ULL << 32) >> __builtin_clz(x - 1);
}

/* get the exponent of a power of 2 (only 32 bit) */
static inline uint32_t pow2_exponent32(uint32_t x)
{
  return 32 - __builtin_clz(x - 1);
}

#endif
