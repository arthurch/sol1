#ifndef IO_H
#define IO_H

#include <io/struct_FILE.h>

#include <stddef.h>
#include <types/bits.h>

/* End Of File code */
#define EOF (-1)

/* buffer modes */
#define _IOFBF 1 /* Fully buffered stream (data wrote in blocks) */
#define _IOLBF 2 /* Line buffered stream (data wrote by lines) */
#define _IONBF 3 /* Unbuffered stream (data wrote as soon as possible) */

/* FILE flags */
#define _IO_USER_BUF          0x0001 /* Don't deallocate buffer on close. */
#define _IO_UNBUFFERED        0x0002
#define _IO_NO_READS          0x0004 /* Reading not allowed.  */
#define _IO_NO_WRITES         0x0008 /* Writing not allowed.  */
#define _IO_EOF_SEEN          0x0010
#define _IO_ERR_SEEN          0x0020
#define _IO_DELETE_DONT_CLOSE 0x0040 /* Don't call close(_fileno) on close.  */
#define _IO_LINKED            0x0080 /* In the list of all open files.  */
#define _IO_IN_BACKUP         0x0100
#define _IO_LINE_BUF          0x0200
#define _IO_TIED_PUT_GET      0x0400 /* Put and get pointer move in unison.  */
#define _IO_CURRENTLY_PUTTING 0x0800
#define _IO_IS_APPENDING      0x1000
#define _IO_IS_FILEBUF        0x2000
                           /* 0x4000  No longer used, reserved for compat.  */
#define _IO_USER_LOCK         0x8000

/* macro functions */
#define CHECK_FILE(F, RET) \
  if((F) == NULL || !_IO_MAGIC_OK((F)->_flags)) { \
    __set_errno(EINVAL); \
    return (RET); \
  }
#define HAS_NO_WRITES(F) \
  ((F)->_flags & _IO_NO_WRITES)

#define HAS_NO_READS(F) \
  ((F)->_flags & _IO_NO_READS)

#define _IO_PUTC(F, C, RET) \
  if((F)->_IO_write_ptr == NULL) { \
    if(io_write_init(F) == EOF) \
      return RET; \
  } \
  if((F)->_IO_write_ptr >= (F)->_IO_write_end) { \
    if(IO_OVERFLOW(F) == EOF) \
      return RET; \
    (F)->_IO_write_ptr = (F)->_IO_write_base; \
  } \
  *((F)->_IO_write_ptr) = (char) (C); (F)->_IO_write_ptr++;

/* macros to access vtable functions */
#define _IO_VTABLE(FILE) \
  (*((struct _IO_jump_t **) ((void *) FILE + FILE->_vtable_offset)))

#define _IO_JUMPS_FUNC(THIS) \
  (IO_validate_vtable(_IO_VTABLE(THIS)))

#define JUMP0(FUNC, THIS) (_IO_JUMPS_FUNC(THIS)->FUNC) (THIS)
#define JUMP1(FUNC, THIS, X1) (_IO_JUMPS_FUNC(THIS)->FUNC) (THIS, X1)
#define JUMP2(FUNC, THIS, X1, X2) (_IO_JUMPS_FUNC(THIS)->FUNC) (THIS, X1, X2)

/* hooks for io vtable */

/* The 'finish' function does any final cleaning up of an _IO_FILE object.
   It does not delete (free) it, but does everything else to finalize it. */
typedef int (*_IO_finish_t) (FILE *);
#define IO_FINISH(THIS) JUMP0(__finish, THIS)

/* The 'overflow' hook flushes the buffer.
  return : non-negative number on succes, EOF on failure */
typedef int (*_IO_overflow_t) (FILE *);
#define IO_OVERFLOW(THIS) JUMP0(__overflow, THIS)

/* The 'underflow' hook tries to fills the get buffer.
   It returns the next character (as an unsigned char) or EOF.  The next
   character remains in the get buffer, and the get position is not changed. */
typedef int (*_IO_underflow_t) (FILE *);
#define IO_UNDERFLOW(THIS) JUMP0(__underflow, THIS)

/* The 'seek' hook moves the stream position to a new position
   relative to the start of the file (if mode==3), the current position
   (mode==1), or the end of the file (mode==2). */
typedef off64_t (*_IO_seek_t) (FILE *fp, off64_t off, int mode);
#define IO_SEEK(THIS, OFF, MODE) JUMP2(__seek, THIS, OFF, MODE)

/* The 'read' hook reads upto n characters into buffer data.
   Returns the number of character actually read. */
typedef int (*_IO_read_t) (FILE *fp, void *data, size_t n);
#define IO_READ(THIS, DATA, N) JUMP2(__read, THIS, DATA, N)

/* The 'write' hook writes upto n characters from buffer data.
   Returns EOF or the number of character actually written. */
typedef int (*_IO_write_t) (FILE *fp, const void *data, size_t n);
#define IO_WRITE(THIS, DATA, N) JUMP2(__write, THIS, DATA, N)

/* The 'close' function close the external file connected to the
  _IO_FILE structure. */
typedef int (*_IO_close_t) (FILE *fp);
#define IO_CLOSE(THIS) JUMP0(__close, THIS)

/* io vtable structure */
struct _IO_jump_t {
  _IO_finish_t __finish;
  _IO_overflow_t __overflow;
  _IO_underflow_t __underflow;
  _IO_seek_t __seek;
  _IO_read_t __read;
  _IO_write_t __write;
  _IO_close_t __close;
};

struct _IO_FILE_plus {
  _IO_FILE_t file;
  const struct _IO_jump_t *vtable;
};
typedef struct _IO_FILE_plus _IO_FILE_plus_t;

/* Initialize a FILE object as stream
  return : out pointer */
FILE *io_stream_init(FILE *out, int flags);

/* prepare a stream to output
  return : 0 on success, EOF on failure */
int io_write_init(FILE *stream);
/* prepare a stream to input
  return : 0 on success, EOF on failure */
int io_read_init(FILE *stream);

/* check if vtable is valid. Terminate process if failure (TODO) */
void io_check_vtable(struct _IO_jump_t *vtable);

/* Standard jump tables */
extern const struct _IO_jump_t _IO_jump_file;

/* Jumpable functions for files */
extern int _IO_file_finish(FILE *fd);
extern int _IO_file_overflow(FILE *fd);
extern int _IO_file_underflow(FILE *fd);
extern off64_t _IO_file_seek(FILE *fd, off64_t off, int mode);
extern int _IO_file_read(FILE *fd, void *data, size_t n);
extern int _IO_file_write(FILE *fd, const void *data, size_t n);
extern int _IO_file_close(FILE *fd);

/* perform vtable pointer validation. If validation fails, terminate
   the process. */
inline const struct _IO_jump_t *IO_validate_vtable(const struct _IO_jump_t *vtable)
{
#ifdef __is_libc

  /* Fast path: The vtable pointer is within the __libc_IO_vtables
     section (will be implement after for user space programs) */
  // uintptr_t section_length = __stop___libc_IO_vtables - __start___libc_IO_vtables;
  // uintptr_t ptr = (uintptr_t) vtable;
  // uintptr_t offset = ptr - (uintptr_t) __start___libc_IO_vtables;
  // if(offset >= section_length)
  //   /* The vtable pointer is not in the expected section. Use the
  //      slow path, which will terminate the process if necessary.  */
  //   io_check_vtable(vtable);
  // return vtable;
  return vtable;

#elif __is_libk

  return vtable;

#elif __is_kernel

  /* kernel is allowed to implement its own vtables (since this function is inline) */
  return vtable;

#endif
}

#endif
