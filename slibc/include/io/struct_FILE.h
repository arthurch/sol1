#ifndef IO_H
#error "struct_FILE.h needs to be accessed only using io.h"
#endif

#ifndef STRUCT_FILE_H
#define STRUCT_FILE_H

#include <stdint.h>

/* _IO_MAGIC number using high 16 bits of _IO_FILE._flags */
#define _IO_MAGIC 0xA2F10000
#define _IO_MAGIC_MASK 0xFFFF0000
/* flags for _IO_FILE structure */
#define _IO_FLAG_ 0x00000001

/* structure representing IO buffer */
struct _IO_FILE {
  /* higher word is _IO_MAGIC, rest is flags */
  uint32_t _flags;
  /* buffering mode (_IOFBF, _IOLBF or _IONBF) */
  int _buf_mode;

  char *_IO_read_ptr; /* current read pointer */
  char *_IO_read_end; /* end of get area */
  char *_IO_read_base; /* start of get area */
  char *_IO_write_ptr; /* current put pointer */
  char *_IO_write_end; /* end of put aera */
  char *_IO_write_base; /* start of put aera */
  char *_IO_buf_base; /* start of reserved space */
  char *_IO_buf_end; /* end of reserved space */

  signed char _vtable_offset; /* offset of io vtable structure */

  /* error indicator */
  int _error;
};
typedef struct _IO_FILE _IO_FILE_t;

typedef struct _IO_FILE FILE;

#define _IO_MAGIC_OK(flag) !((flag & _IO_MAGIC_MASK) ^ _IO_MAGIC)

#endif
