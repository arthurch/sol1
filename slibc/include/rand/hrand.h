#ifndef HRAND_H
#define HRAND_H

#include <stdint.h>

extern uint32_t _hardware_rand_supported;

int hrand_supported();
uint32_t hrand32();

#endif
