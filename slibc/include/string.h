#ifndef STRING_H
#define STRING_H

#include <types/bits.h>

char *itoa(int value, char *buffer, int base);
char *itoa32(uint32_t value, char *buffer, int base);

int memcmp(const void*, const void*, size_t);
void *memcpy(void* __restrict, const void* __restrict, size_t);
void *memmove(void*, const void*, size_t);
void *memset(void*, int, size_t);

int strcmp(const char *str1, const char *str2);
char *strcat(char *dest, const char *src);
char *strcpy(char *dest, const char *src);
size_t strlen(const char *s);

#endif
