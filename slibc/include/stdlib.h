#ifndef STDLIB_H
#define STDLIB_H

#include <malloc.h>

void __attribute__((noreturn)) abort(void);

#endif
