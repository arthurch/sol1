#ifndef BITS_H
#define BITS_H

#include <stdint.h>

#undef NULL
#define NULL ((void *)0)

typedef uint32_t size_t;

typedef int64_t off64_t;
typedef int32_t off32_t;

#endif
