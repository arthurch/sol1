#ifndef TREE_H
#define TREE_H

#include <types/bits.h>
#include <list.h>

typedef uint8_t (*tree_compare_t)(void *v1, uintptr_t v2);

struct tree_node {
  void *value;
  list_t *children;
  struct tree_node *parent;

  struct tree *owner;
};
typedef struct tree_node tree_node_t;

struct tree {
  size_t length;

  tree_node_t *root;
};
typedef struct tree tree_t;

tree_t *tree_create();
tree_node_t *tree_create_node(void *val);

void tree_append_node(tree_node_t *parent, tree_node_t *node);
tree_node_t *tree_append(tree_node_t *parent, void *val);

void tree_remove(tree_node_t *node);
void tree_destroy(tree_t *tree);
void tree_empty(tree_t *tree);

void tree_root_node(tree_t *tree, tree_node_t *root);
tree_node_t *tree_root(tree_t *tree, void *val);

tree_node_t *tree_iterate_children(tree_node_t *parent);

tree_node_t *tree_node_find(tree_node_t *parent, void *val);
tree_node_t *tree_find(tree_t *tree, void *val);

tree_node_t *tree_node_find_custom(tree_node_t *parent, uintptr_t val, tree_compare_t compare);
tree_node_t *tree_find_custom(tree_t *tree, uintptr_t val, tree_compare_t compare);

void tree_dump(tree_t *tree);
void tree_dump_node(tree_node_t *node, size_t spacing);

#endif
