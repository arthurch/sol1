#ifndef STDIO_H
#define STDIO_H

#include <io/io.h>

#include <stddef.h>
#include <stdarg.h>

#include <errno.h>

/* size macros */
#define BUFSIZ 8192 /* default maximum buffer size */
#define FOPEN_MAX 8 /* number of file that may be open simultaneously */

/* null pointer */
#define NULL ((void *) 0)

/* seek modes */
#define SEEK_CUR 1 /* seek from current position */
#define SEEK_END 2 /* seek from end of file */
#define SEEK_SET 3 /* seek from beginning of file */

/* standart input/output streams */
extern FILE *stdout;
extern FILE *stdin;
extern FILE *stderr;

/* POSIX functions to manipulate streams or files */

/* stream manipulation */

/* set stream buffer with its mode.
  return : 0 on success, negative number on failure */
int setvbuf(FILE *stream, char *buf, int mode, size_t size);

/* flush stream (set get and put pointers to NULL).
  return : 0 on success, EOF on failure */
int fflush(FILE *stream);

/* stream input */

/* get byte from stream.
  return : byte read on success, EOF on failure */
int fgetc(FILE *stream);
#define getc fgetc
/* get bytes from stream, stored in str buffer, with count as maximum length
  return : str on success, null pointer on failure */
char *fgets(char *str, int count, FILE *stream);
/* get byte from stdin (equivalent to : getc(stdin)) */
int getchar();

/* stream output */

/* put byte to stream - use fputs for larger inputs (for optimization purpose).
  return : written character on success, EOF on failure */
int fputc(FILE *stream, int c);
#define putc fputc
/* write bytes to stream, stored in str buffer (null terminated)
  return : number of written bytes on success, EOF on failure */
int fputs(const char *str, FILE *stream);
/* put byte to stdout (equivalent to : putc(stdout, c)) */
int putchar(int c);

/* formatted io functions */

#include <_printf/printf.h>

/* ... scanf(); */

#endif
