#include <io/io.h>

FILE *io_stream_init(FILE *out, int flags)
{
  out->_flags = (_IO_MAGIC & _IO_MAGIC_MASK) | flags;

  out->_buf_mode = _IOFBF;

  out->_IO_read_ptr = NULL;
  out->_IO_read_end = NULL;
  out->_IO_read_base = NULL;
  out->_IO_write_ptr = NULL;
  out->_IO_write_end = NULL;
  out->_IO_write_base = NULL;
  out->_IO_buf_base = NULL;
  out->_IO_buf_end = NULL;

  out->_vtable_offset = sizeof(struct _IO_FILE);

  out->_error = 0;

  return out;
}
