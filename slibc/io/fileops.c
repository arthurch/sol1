#include <stdio.h>

extern int tty_puts(const char *s);
extern int tty_putc(int c);

/* TODO : function for file stream */
int _IO_file_finish(FILE *fd)
{
  (void) fd;
  return 0;
}

int _IO_file_overflow(FILE *fd)
{
  tty_puts(" - overflow:\n");
  char *ptr = fd->_IO_write_base;
  for(; ptr < fd->_IO_write_ptr; ptr++)
  {
    tty_putc(*ptr);
  }
  tty_putc('\n');
  return 5;
}

int _IO_file_underflow(FILE *fd)
{
  (void) fd;
  return 0;
}

off64_t _IO_file_seek(FILE *fd, off64_t off, int mode)
{
  (void) fd; (void) off; (void) mode;
  return 0;
}

int _IO_file_read(FILE *fd, void *data, size_t n)
{
  (void) fd; (void) data; (void) n;
  return 0;
}

int _IO_file_write(FILE *fd, const void *data, size_t n)
{
  (void) fd; (void) data; (void) n;
  return 0;
}

int _IO_file_close(FILE *fd)
{
  (void) fd;
  return 0;
}

/* vtable for classic file streams */
const struct _IO_jump_t _IO_jump_file = {
  _IO_file_finish,
  _IO_file_overflow,
  _IO_file_underflow,
  _IO_file_seek,
  _IO_file_read,
  _IO_file_write,
  _IO_file_close
};
