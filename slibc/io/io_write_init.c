#include <io/io.h>

#include <stdio.h>

int io_write_init(FILE *stream)
{
  CHECK_FILE(stream, EOF);

  if(HAS_NO_WRITES(stream))
  {
    __set_errno(EBADF);
    return EOF;
  }

  if(stream->_IO_buf_base == NULL)
  {
    setvbuf(stream, NULL, _IOFBF, BUFSIZ);
  }

  stream->_IO_write_base = stream->_IO_buf_base;
  stream->_IO_write_end = stream->_IO_buf_end;
  stream->_IO_write_ptr = stream->_IO_write_base;

  return 0;
}
