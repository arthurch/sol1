#include <stdio.h>

int setvbuf(FILE *fd, char *buf, int mode, size_t size)
{
  if(!_IO_MAGIC_OK(fd->_flags))
  {
    io_stream_init(fd, 0);
  }

  if(mode == _IOFBF)
  {
    fd->_buf_mode = _IOFBF;
  } else if(mode == _IOLBF)
  {
    fd->_buf_mode = _IOLBF;
  } else if(mode == _IONBF)
  {
    fd->_buf_mode = _IONBF;
  } else
  {
    __set_errno(EINVAL);
    return -1;
  }

  if(buf == 0)
  {
    // TODO : malloc buffer
  }

  fd->_IO_buf_base = buf;
  fd->_IO_buf_end = buf + size;

  return 0;
}
