#include <io/io.h>

#include <stdio.h>

int io_read_init(FILE *stream)
{
  CHECK_FILE(stream, EOF);

  if(stream->_IO_buf_base == 0)
  {
    setvbuf(stream, NULL, _IOFBF, BUFSIZ);
  }

  stream->_IO_read_base = stream->_IO_buf_base;
  stream->_IO_read_end = stream->_IO_buf_end;
  stream->_IO_read_ptr = stream->_IO_read_base;

  return 0;
}
