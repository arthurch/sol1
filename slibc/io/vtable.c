#include <io/io.h>

void io_check_vtable(struct _IO_jump_t *vtable)
{
  (void) vtable;
#ifdef __is_libk
  /* libk version */
#elif __is_libc
  /* libc version */
#endif
}
