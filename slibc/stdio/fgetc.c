#include <stdio.h>

int fgetc(FILE *fd)
{
  (void) fd;
  return 0;
}

int getchar()
{
  return fgetc(stdin);
}
