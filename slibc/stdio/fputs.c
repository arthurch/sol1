#include <stdio.h>
#include <string.h>

int fputs(const char *str, FILE *stream)
{
  CHECK_FILE(stream, EOF);
  if(HAS_NO_WRITES(stream))
  {
    __set_errno(EBADF);
    return EOF;
  }

  size_t len = strlen(str);

  unsigned int i;
  for(i = 0; i<len; i++)
  {
    _IO_PUTC(stream, str[i], EOF);
  }

  return i;
}
