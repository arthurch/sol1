#include <stdio.h>

int fflush(FILE *stream)
{
  CHECK_FILE(stream, EOF);
  if(IO_OVERFLOW(stream) == EOF)
    return EOF;
  /* IO_OVERFLOW should flush the stream buffer.
  we can assume that the buffer is empty */

  /* setting get and put pointers to NULL so
  they will be reset after */
  stream->_IO_write_ptr = NULL;
  stream->_IO_read_ptr = NULL;

  return 0;
}
