#include <stdio.h>

int fputc(FILE *stream, int c)
{
  CHECK_FILE(stream, EOF);
  if(HAS_NO_WRITES(stream))
  {
    __set_errno(EBADF);
    return EOF;
  }

  _IO_PUTC(stream, c, EOF);

  return c;
}

int putchar(int c)
{
  return fputc(stdout, c);
}
