#include <rand/hrand.h>

uint32_t _hardware_rand_supported;

extern uint32_t _hardware_rand32();

int hrand_supported()
{
  return _hardware_rand_supported & 1;
}

uint32_t hrand32()
{
  if(!hrand_supported())
    return 0;

  uint32_t r;
  r = _hardware_rand32();

  return r;
}
