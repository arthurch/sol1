### INCLUDE DIR ###
LIBC_INCLUDE_DIR:=include

### DEFAULT FLAGS ###
CFLAGS?=-O2 -g
LFLAGS?=-O2

### BUILDING FLAGS ###
CFLAGS:=$(CFLAGS) -std=gnu11 -ffreestanding -Wall -Wextra
LIBS:=$(LIBS) -nostdlib -lgcc

### CFLAGS FOR EACH LIBK AND LIBC ###
LIBC_CFLAGS:=$(CFLAGS)
LIBK_CFLAGS:=$(CFLAGS)

### BUILDING MACROS ###
LIBC_CFLAGS:=$(LIBC_CFLAGS) -D __is_libc
LIBK_CFLAGS:=$(LIBK_CFLAGS) -D __is_libk

### SUB DIRS ###
ARCHDIR:=arch/$(ARCH)
MAKE_CONFIG_FILE=config.makefile
MAKE_INCLUDES:=$(ARCHDIR)/$(MAKE_CONFIG_FILE)

include $(MAKE_INCLUDES)

### OBJS TO BE BUILD ###
OBJS:=\
	ssp/ssp.o \
	stdio/printf.o stdio/stdio.o stdio/fflush.o stdio/fputc.o stdio/fputs.o stdio/fgetc.o stdio/fgets.o \
	string/itoa.o string/strlen.o string/memcmp.o string/memcpy.o string/memmove.o string/memset.o string/strcat.o string/strcpy.o string/strcmp.o \
	stdlib/abort.o \
	misc/list.o misc/bitset.o misc/tree.o \
	io/io_stream_init.o io/fileops.o io/setvbuf.o io/io_read_init.o io/io_write_init.o io/vtable.o \
	rand/hrand.o \
	acmalloc/malloc.o
LIBC_OBJS:=\
	$(LIBC_ARCH_OBJS)\
	$(OBJS)
LIBK_OBJS:=\
	$(LIBK_ARCH_OBJS)\
	$(OBJS)
LIBK_OBJS:=$(LIBK_OBJS:.o=.libk.o)

### OUTPUT FILE ###
LIBC_OUTPUT?=libc.a
LIBK_OUTPUT?=libk.a
LIBC_ALL_OUTPUTS:=$(LIBK_OUTPUT)

### BUILDING COMMANDS ###
.PHONY: all install install-headers clean cleanall

all: $(OUTPUT)

install: $(LIBC_ALL_OUTPUTS)
	@$(ECHON) installing libc to $(LIB_DIR)...
	@mkdir -p $(LIB_DIR_SYS)
	@$(CP) $(LIBC_ALL_OUTPUTS) $(LIB_DIR_SYS)/.
	@echo done

install-headers:
	@$(ECHON) installing headers from libc to $(INCLUDE_DIR)...
	@mkdir -p $(INCLUDE_DIR_SYS)
	@$(CP) -R $(LIBC_INCLUDE_DIR)/. $(INCLUDE_DIR_SYS)/.
	@echo done

### LIBRARIES ###
$(LIBC_OUTPUT): $(LIBC_OBJS)
	$(AR) -rcs $@ $?

$(LIBK_OUTPUT): $(LIBK_OBJS)
	$(AR) -rcs $@ $?

### LIBC FILES TO BE BUILD ###
%.o: %.c Makefile
	$(CC) -MD $(LIBC_CFLAGS) -c $< -o $@
# gnu as version
%.o: %.S Makefile
	$(AS) -c $< -o $@

### LIBK FILES TO BE BUILD ###
%.libk.o: %.c Makefile
	$(CC) -MD $(LIBK_CFLAGS) -c $< -o $@
# gnu as version
%.libk.o: %.S Makefile
	$(AS) -c $< -o $@

### CLEANING ###
clean:
	@$(ECHON) cleaning libc...
	@$(RM) -f $(LIBC_OBJS)
	@$(RM) -f $(LIBC_OBJS:.o=.d)
	@$(RM) -f $(LIBK_OBJS)
	@$(RM) -f $(LIBK_OBJS:.o=.d)
	@echo done

cleanall: clean
	@$(ECHON) cleaning built libraries...
	@$(RM) -f $(LIBC_ALL_OUTPUTS)
	@echo done

-include $(LIBC_OBJS:.o=.d)
-include $(LIBK_OBJS:.libk.o=.libk.d)
