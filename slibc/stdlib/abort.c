#include <stdlib.h>

#ifdef __is_libk
extern void _panic_halt_loop();
#endif

void __attribute__((noreturn)) abort(void)
{
#ifdef __is_libk
  _panic_halt_loop();
#elif __is_libc
  // ABORT LIBC FUNCTION
  while(1)
  {

  }
#endif
  __builtin_unreachable();
}
