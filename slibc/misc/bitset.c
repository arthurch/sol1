#include <bitset.h>

#include <string.h>
#include <malloc.h>

#include <stdio.h>

bitset_t *bitset_create(size_t sz)
{
  sz = (sz + BITSET_WORD_MASK) / BITSET_WORD_SIZE;
  bitset_t *bitset = (bitset_t *) malloc(sizeof(bitset_t));
  bitset->data = (uint32_t *) malloc(sz * sizeof(uint32_t));
  memset(bitset->data, 0, sz * sizeof(uint32_t));
  bitset->nwords = sz;
  bitset->length = sz * BITSET_WORD_SIZE;

  return bitset;
}

void bitset_set(bitset_t *bitset, size_t index)
{
  if(index >= bitset->length) return;

  bitset->data[BITINDEX(index)] |= 0x1 << BITOFFSET(index);
}

void bitset_clear(bitset_t *bitset, size_t index)
{
  if(index >= bitset->length) return;

  bitset->data[BITINDEX(index)] &= ~(0x1 << BITOFFSET(index));
}

uint8_t bitset_test(bitset_t *bitset, size_t index)
{
  if(index >= bitset->length) return 0;

  return bitset->data[BITINDEX(index)] << BITOFFSET(index);
}

/*
 * Get the first element which is not zero
 * @return index found, or length if nothing
 * is found
 */
size_t bitset_get_first_clear(bitset_t *bitset)
{
  unsigned int i;
  for(i = 0; i<bitset->nwords; i++)
  {
    if(bitset->data[i] != (0xFFFFFFFF)) /* if one or more bit is zero */
    {
      uint32_t word = bitset->data[i];
      unsigned int j;
      uint8_t found;
      for(j = 0; j<BITSET_WORD_SIZE; j++)
      {
        found = ~((word >> j) & 0x1) & 0x1;
        if(found)
        {
          return (i * BITSET_WORD_SIZE + j);
        }
      }
    }
  }

  return bitset->length;
}
