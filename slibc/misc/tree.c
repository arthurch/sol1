#include <tree.h>

#include <malloc.h>
#include <stdio.h>

tree_t *tree_create()
{
  tree_t *tree = (tree_t *) malloc(sizeof(tree_t));
  tree->length = 0;
  tree->root = NULL;

  return tree;
}

tree_node_t *tree_create_node(void *val)
{
  tree_node_t *node = (tree_node_t *) malloc(sizeof(tree_node_t));
  node->value = val;
  node->children = list_create();
  node->parent = NULL;

  return node;
}

void tree_append_node(tree_node_t *parent, tree_node_t *node)
{
  if(__builtin_expect(node == NULL, 0) || __builtin_expect(parent == NULL, 0))
  {
    return;
  }

  list_append(parent->children, node);
  node->parent = parent;
  node->owner = parent->owner;
  node->owner->length++;
}

void tree_remove(tree_node_t *node)
{
  if(__builtin_expect(node == NULL, 0))
  {
    return;
  }

  /* removing children */
  node_t *n = list_start_iterating(node->children);
  for(; n != NULL; n = list_iterate(node->children))
  {
    tree_remove((tree_node_t *) n->value);
  }
  list_free(node->children);

  /* removing me */
  tree_node_t *parent = node->parent;
  if(parent != NULL)
  {
    list_remove(parent->children, node);
  }
  if(node == node->owner->root)
  {
    /* node is root */
    node->owner->root = NULL;
  }
  node->owner->length--;

  free(node);
}

void tree_empty(tree_t *tree)
{
  tree_remove(tree->root);
  tree->root = NULL;
}

void tree_destroy(tree_t *tree)
{
  tree_empty(tree);
  free(tree);
}

void tree_root_node(tree_t *tree, tree_node_t *root)
{
  tree->root = root;
  root->owner = tree;
  tree->length++;
}

tree_node_t *tree_root(tree_t *tree, void *val)
{
  tree_node_t *node = tree_create_node(val);
  tree_root_node(tree, node);
  return node;
}

tree_node_t *tree_append(tree_node_t *parent, void *val)
{
  tree_node_t *node = tree_create_node(val);
  tree_append_node(parent, node);
  return node;
}

tree_node_t *tree_iterate_children(tree_node_t *parent)
{
  node_t *next = list_iterate(parent->children);
  if(next == NULL)
    return NULL;
  return (tree_node_t *) next->value;
}

tree_node_t *tree_node_find(tree_node_t *parent, void *val)
{
  if(parent->value == val)
  {
    return parent;
  }

  list_reset_iterating(parent->children);
  tree_node_t *node = tree_iterate_children(parent);
  for(; node != NULL; node = tree_iterate_children(parent))
  {
    tree_node_t *found = tree_node_find(node, val);
    if(found != NULL) return found;
  }

  return NULL;
}

tree_node_t *tree_find(tree_t *tree, void *val)
{
  return tree_node_find(tree->root, val);
}

tree_node_t *tree_node_find_custom(tree_node_t *parent, uintptr_t val, tree_compare_t compare)
{
  if(compare(parent->value, val))
  {
    return parent;
  }

  list_reset_iterating(parent->children);
  tree_node_t *node = tree_iterate_children(parent);
  for(; node != NULL; node = tree_iterate_children(parent))
  {
    tree_node_t *found = tree_node_find_custom(node, val, compare);
    if(found != NULL) return found;
  }

  return NULL;
}

tree_node_t *tree_find_custom(tree_t *tree, uintptr_t val, tree_compare_t compare)
{
  return tree_node_find_custom(tree->root, val, compare);
}

void tree_dump(tree_t *tree)
{
  if(__builtin_expect(tree == NULL, 0))
  {
    return;
  }

  if(tree->root != NULL)
  {
    printf("tree_dump: %p, length=%zu, root=%p:%p\n", tree, tree->length, tree->root, tree->root->value);
  } else
  {
    printf("tree_dump: %p, length=%zu, no root\n", tree, tree->length);
    return;
  }
  tree_dump_node(tree->root, 1);
}

void tree_dump_node(tree_node_t *node, size_t spacing)
{
  unsigned int i;
  foreach(node->children, n)
  {
    printf("tree_dump:");
    for(i = 0; i<spacing; i++)
    {
      printf("  ");
    }
    printf("-> %p:%p\n", n->value, ((tree_node_t *) n->value)->value);
    tree_dump_node((tree_node_t *) n->value, spacing + 1);
  }
}
