#include <string.h>

void *memset(void *ptr, int val, size_t sz)
{
  if(sz)
  {
    unsigned char *dst = (unsigned char *) ptr;
    while(sz--)
    {
      dst[sz] = (unsigned char) val;
    }
  }
  return ptr;
}
