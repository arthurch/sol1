#include <string.h>

void *memcpy(void *__restrict dptr, const void *__restrict sptr, size_t sz)
{
  unsigned char *dest = (unsigned char *) dptr;
  const unsigned char *src = (const unsigned char *) sptr;
  for(size_t i = 0; i<sz; i++)
    dest[i] = src[i];
  return dptr;
}
