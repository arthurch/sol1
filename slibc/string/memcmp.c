#include <string.h>

int memcmp(const void* aptr, const void* bptr, size_t sz)
{
  const unsigned char *a = (const unsigned char *) aptr;
  const unsigned char *b = (const unsigned char *) bptr;
  unsigned int i;
  for(i = 0; i<sz; i++)
  {
    if(a[i] < b[i])
      return -1;
    else if(a[i] > b[i])
      return 1;
  }
  
  return 0;
}
