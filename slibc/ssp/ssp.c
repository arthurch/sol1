#include <stdint.h>
#include <stdlib.h>

/* default values for __stack_chk_guard,
  if RDRAND is supported, it will be randomly
  generated at startup */
#if UINT32_MAX == UINTPTR_MAX
#define STACK_CHK_GUARD 0xf456a2ce
#else
#define STACK_CHK_GUARD 0xfda4231cefee43a2
#endif

#ifdef __is_libk // TODO replace by an kernel panic handler
extern void _panic_halt_loop();
#endif

uintptr_t __stack_chk_guard = STACK_CHK_GUARD;

__attribute__((noreturn))
void __stack_chk_fail(void)
{
#if __is_libc
  abort();
#elif __is_libk
  _panic_halt_loop();
#endif
  __builtin_unreachable();
}
