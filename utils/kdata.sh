#!/bin/bash

#default : arch = i386, obj file format = elf32-i386

if [[ $# -eq 2 ]]; then
  objcopy -v -O elf32-i386 -B i386 -I binary $1 $2
elif [[ $# -eq 4 ]]; then
  objcopy -v -O $2 -B $1 -I binary $3 $4
else
  echo "usage : $0 [arch] [obj file format] <input bin file> <output obj file>"
fi
