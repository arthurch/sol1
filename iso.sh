#!/bin/bash

if [ -z $CONF_DONE ]; then
  . ./configure.sh
fi

KER_BIN=${BOOT_DIR}/${OUTPUT}

if grub-file --is-x86-multiboot $KER_BIN; then
  echo multiboot confirmed
else
  echo [error] the file $KER_BIN is not multiboot ready
  exit 1
fi

mkdir -p isodir/boot/grub/

cp $KER_BIN isodir/boot/${OUTPUT}
cp $GRUB_CFG isodir/boot/grub/${GRUB_CFG}

ISO_FILE=`basename -a -s .ker ${KER_BIN}`.iso
grub-mkrescue -o ${ISO_FILE} isodir
