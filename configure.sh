#!/bin/bash

echo -n "Configuring sol1... "

PROJDIR="slibc solk"
SYS_PROJDIR_HEADERS="slibc solk"

### TARGET HOST AND ARCH ###
export HOST=i686-elf
export ARCH=i386

### BUILDING OPTIONS ###
ADD_SYS_INCLUDES=1

### OUTPUT BINARIES ###
export OUTPUT=sol1.ker
export GRUB_CFG=grub.cfg

### BUILDING MACROS ###
export CC=${HOST}-gcc
export AS=${HOST}-as
export LD=${HOST}-ld
export AR=${HOST}-ar
export OBJCOPY=${HOST}-objcopy

### BUILDING FLAGS ###
export CFLAGS="-O2 -g -fstack-protector"
export LFLAGS="-O2"
export LIBS=""

### UTILITIES ###
export CP="cp -u --preserve=timestamps"
export RM="rm"
export ECHO="echo [info]"
export ECHON="echo -n [info]"

### SYSROOT DIR ###
export SYSROOT="$(pwd)/sysroot"
export CC="${CC} --sysroot=${SYSROOT}"

### BUILDING DIR ###
export PREFIX="/usr"
export INCLUDE_DIR="${PREFIX}/include"
export INCLUDE_DIR_SYS="${SYSROOT}${INCLUDE_DIR}"
export LIB_DIR="${PREFIX}/lib"
export LIB_DIR_SYS="${SYSROOT}${LIB_DIR}"
export BOOT_DIR="${SYSROOT}/boot"

if [[ ADD_SYS_INCLUDES -eq 1 ]]; then
  export CC="${CC} -isystem=${INCLUDE_DIR}"
fi

CONF_DONE=1
echo "OK"
