#!/bin/bash

if [ -z $CONF_DONE ]; then
  . ./configure.sh
fi

for PROJECT in ${PROJDIR}; do
  (cd ${PROJECT} && make install-headers)
done
