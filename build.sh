#!/bin/bash

. ./configure.sh
. ./headers.sh


# 0: Exit status is 'Successful'
# 2: Make Encountered Errors

for PROJECT in ${PROJDIR}; do
  echo [info] building $PROJECT...
  (cd ${PROJECT} && make install)
  RET=$?
  echo [info] return code : $RET
  if [ $RET -eq "2" ]; then
    echo [error] make encountered errors, stopping execution.
    exit 2
  fi
  echo [info] $PROJECT done
done

if [[ $# -ge 1 ]]; then
  if [[ $1 -eq "c" ]]; then
    . ./clean.sh
  fi
fi
