#!/bin/bash

SOFT_CLEAN=1
QEMU_ARGS="-m 3G -d int -d guest_errors -d cpu_reset" # available options : -no-reboot -no-shutdown -d cpu_reset

. ./build.sh
RET=$?
if [[ RET -eq 2 ]]; then
  exit
fi

if [[ $SOFT_CLEAN -eq 1 ]]; then
  . ./clean.sh soft
fi

. ./iso.sh
RET=$?
if [[ RET -eq 1 ]]; then
  exit
fi

DBG_OPTION="-s -S"
if [[ $# -ge 1 ]]; then
  if [[ $1 -eq "db" ]]; then
    QEMU_ARGS="${QEMU_ARGS} ${DBG_OPTION}"
  elif [[ $1 -eq "dbg" ]]; then
    QEMU_ARGS="${QEMU_ARGS} ${DBG_OPTION}"
  elif [[ $1 -eq "gdb" ]]; then
    QEMU_ARGS="${QEMU_ARGS} ${DBG_OPTION}"
  fi
fi

if [[ -f $ISO_FILE ]]; then
  QEMU_ARGS="${QEMU_ARGS} -cdrom ${ISO_FILE}"
  echo "executing qemu : ${QEMU_ARGS}"
  qemu-system-i386 ${QEMU_ARGS}
else
  echo [error] no iso file found
fi
